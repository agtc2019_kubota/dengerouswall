/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: Wall.h

+ ------ Explanation of file --------------------------------------------------------------------------
       
	壁のギミックを担当するクラス

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

class Wall{
	public :
		Wall() ;
		~Wall() ;
		void Init( int ) ;
		void re_Init( int ) ;
		void SetMoveRotation( int ) ;

		VECTOR	w_pos ;			// --- ウォールポジション
		float	w_spd ;			// --- ウォールスピード(移動量)
		float	w_rot ;			// --- ウォールローテーション(8方向)
		float	sw_spd ;		// --- シャドウスピード(移動量)
		float   scale_y ;		// --- シャドウスケール
		int		wall_model ;
		int		w_Model ;		// --- 壁情報格納関数

		float   trans_shadow  ; // --- 影の透明度の設定
		float	tsw_spd ;		// --- 
} ;

void SetWallType() ;
void Shadow_Move( int ) ;

