
/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: Player.cpp

+ ------ Explanation of file --------------------------------------------------------------------------
       
	ポジション・向き・アニメーション関連の変数宣言・初期セット

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

#include "Common.h"

/* ===============================================================================
|
|   関数名	Player
|       処理内容  コンストラクタ
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
Player::Player(){

}

/* ===============================================================================
|
|   関数名	~Player
|       処理内容  デストラクタ
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
Player::~Player(){


}

/* ===============================================================================
|
|   関数名	p_Init
|       処理内容  モデルハンドルの格納や初期座標のセット
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Player::p_Init( int p_No )
{
	// --- 座標 ---------------------------------------------------------------
	switch ( p_No ){
		case PLAYER1 :
			death_count = 0 ;
			press_count = 0 ;
			attack_count = 0 ;
			char_pos =  VGet( 150.0f , 840.0f , -250.0f ) ;		// プレイヤー1の初期座標
			switch( p_flg ){
				case PLAYER1 :
					player_model =  MV1LoadModel(PLAYER_MODEL_1) ;	
					rootflm = MV1SearchFrame(player_model,"obj_2") ;								// --- ボーンの番号を返す
					p_Model = player_model ;														// --- モデル情報格納

					anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Stay.mv1" ) ;	// --- 静止アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_run = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Run.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_atk = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Atk.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_down = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Nock.mv1" ) ;		// --- ノックバックアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_press = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Press.mv1" ) ;	// --- 潰れアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_win = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Happy.mv1" ) ;		// --- 勝利アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（1）アニメーション
					anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（2）アニメーション
					anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ
					break ;

				case PLAYER2 :
					player_model =  MV1LoadModel(PLAYER_MODEL_2) ;	
					rootflm = MV1SearchFrame(player_model,"obj_2") ;								// --- ボーンの番号を返す
					p_Model = player_model ;														// --- モデル情報格納

					anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Stay.mv1" ) ;	// --- 静止アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_run = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Run.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_atk = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Atk.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_down = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Nock.mv1" ) ;		// --- ノックバックアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_press = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Press.mv1" ) ;	// --- 潰れアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_win = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Happy.mv1" ) ;		// --- 勝利アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（1）アニメーション
					anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（2）アニメーション
					anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ
					break ;

				case PLAYER3 :
					player_model =  MV1LoadModel(PLAYER_MODEL_3) ;	
					rootflm = MV1SearchFrame(player_model,"obj_2") ;								// --- ボーンの番号を返す
					p_Model = player_model ;														// --- モデル情報格納

					anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Stay.mv1" ) ;	// --- 静止アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_run = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Run.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_atk = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Atk.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_down = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Nock.mv1" ) ;		// --- ノックバックアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_press = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Press.mv1" ) ;	// --- 潰れアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_win = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Happy.mv1" ) ;		// --- 勝利アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（1）アニメーション
					anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（2）アニメーション
					anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ
					break ;
			}
			break ;

		case PLAYER2 :
			char_pos =  VGet( 650.0f , 840.0f , -250.0f ) ;		// プレイヤー2の初期座標

			switch( p_flg ){
				case PLAYER1 :
					player_model =  MV1LoadModel(PLAYER_MODEL_1) ;	
					rootflm = MV1SearchFrame(player_model,"obj_2") ;								// --- ボーンの番号を返す
					p_Model = player_model ;														// --- モデル情報格納

					anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Stay.mv1" ) ;	// --- 静止アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_run = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Run.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_atk = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Atk.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_down = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Nock.mv1" ) ;		// --- ノックバックアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_press = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Press.mv1" ) ;	// --- 潰れアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_win = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Happy.mv1" ) ;		// --- 勝利アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（1）アニメーション
					anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（2）アニメーション
					anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ
					break ;

				case PLAYER2 :
					player_model =  MV1LoadModel(PLAYER_MODEL_2) ;	
					rootflm = MV1SearchFrame(player_model,"obj_2") ;								// --- ボーンの番号を返す
					p_Model = player_model ;														// --- モデル情報格納

					anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Stay.mv1" ) ;	// --- 静止アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_run = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Run.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_atk = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Atk.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_down = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Nock.mv1" ) ;		// --- ノックバックアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_press = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Press.mv1" ) ;	// --- 潰れアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_win = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Happy.mv1" ) ;		// --- 勝利アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（1）アニメーション
					anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（2）アニメーション
					anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ
					break ;

				case PLAYER3 :
					player_model =  MV1LoadModel(PLAYER_MODEL_3) ;	
					rootflm = MV1SearchFrame(player_model,"obj_2") ;								// --- ボーンの番号を返す
					p_Model = player_model ;														// --- モデル情報格納

					anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Stay.mv1" ) ;	// --- 静止アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_run = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Run.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_atk = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Atk.mv1" ) ;		// --- 走りアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_down = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Nock.mv1" ) ;		// --- ノックバックアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_press = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Press.mv1" ) ;	// --- 潰れアニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					anim_win = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Happy.mv1" ) ;		// --- 勝利アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（1）アニメーション
					anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ

					// --- プレイヤー1の敗北（2）アニメーション
					anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
					tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;						// --- モデルにアタッチ
					anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;		// --- アニメーション総時間取得
					MV1DetachAnim(player_model,tmp_attachidx) ;										// --- アニメーションをデタッチ
					break ;
			}
			break ;
	}

	tmp_attachidx = MV1AttachAnim(p_Model,0,anim_nutral) ;
	anim_totaltime = MV1GetAttachAnimTotalTime(p_Model, tmp_attachidx) ;
	// --- アニメーションして動いてもその場で動いてるような状態
	MV1SetFrameUserLocalMatrix(p_Model,rootflm,MGetIdent()) ;
	anim_spd = 1.0f ;

	invi_flg = 0 ;

}
/* ===============================================================================
|
|   関数名	GameStartSpin
|       処理内容  ゲーム画面に入った際の演出
+ --------------------------------------------------------------------------------
|   IN --->  p_No      --> プレイヤー識別番号
|   OUT -->            --> 
+ ============================================================================= */
void Player::GameStartSpin( int M_Game_Count_SE, int p_No )
{
p_Landing_flg = 0 ; // 着地したかのフラグ 

	if( startFlg == false )
	{
		switch ( p_No ){
			case PLAYER1 :
				{
					static BOOL sound_f = false ;

					if ( sound_f == false ){
						sound_f = true ;
						PlaySoundMem( M_Game_Count_SE, DX_PLAYTYPE_BACK, TRUE ) ;
					}
				}
				// カメラ位置
				cpos = VGet( 370.0f , 400.0f ,-380.0f ) ;
				// カメラ注視点
				ctgt = VGet( m_Player[PLAYER1].char_pos.x -150.0f , m_Player[PLAYER1].char_pos.y + 100.0f  , m_Player[PLAYER1].char_pos.z + 150.0f ) ;
				SetCameraPositionAndTargetAndUpVec(cpos,ctgt,VGet(0.0f,1.0f,0.0f)) ;

				char_pos.y -= 7.0f ;
				char_rot += 0.1f ;
				timer.Direct_Timer() ;
				if( char_pos.y <= 300.0f  )
				{
					char_pos.y = 300.0f ;
					char_rot = eRight ;

					if( timer.d_time >= 2 )
					{
						p_Landing_flg = 1 ;
					}
				}
				break ;

			case PLAYER2 :

				if(m_Player[PLAYER1].p_Landing_flg == 1  )
				{
					{
						static BOOL sound_f = false ;

						if ( sound_f == false ){
							sound_f = true ;
							PlaySoundMem( M_Game_Count_SE, DX_PLAYTYPE_BACK, TRUE ) ;
						}
					}
					// カメラ位置
					cpos = VGet( 370.0f ,400.0f , -380.0f ) ;
					// カメラ注視点
					ctgt = VGet( m_Player[PLAYER2].char_pos.x +150.0f , m_Player[PLAYER2].char_pos.y + 100.0f  , m_Player[PLAYER2].char_pos.z + 150.0f ) ;
					SetCameraPositionAndTargetAndUpVec(cpos,ctgt,VGet(0.0f,1.0f,0.0f)) ;
/*

					// ---めっちゃいいやつ --- //
					// カメラ位置
					cpos = VGet( 370.0f , m_Player[PLAYER1].char_pos.y + 70.0f , -380.0f ) ;
					// カメラ注視点
					ctgt = VGet( m_Player[PLAYER1].char_pos.x -150.0f , m_Player[PLAYER2].char_pos.y + 100.0f , m_Player[PLAYER1].char_pos.z + 150.0f ) ;
					SetCameraPositionAndTargetAndUpVec(cpos,ctgt,VGet(0.0f,1.0f,0.0f)) ;

*/
					char_pos.y -= 7.0f ;
					char_rot -= 0.1f ;
					timer.Direct_Timer() ;

					if( char_pos.y <= 300.0f )
					{
						char_pos.y = 300.0f ;
						char_rot = eLeft ;
					
						if( timer.d_time >= 6 )
						{
							p_Landing_flg = 1 ;
						}
						
					}
				}

				break ;
		}
	}
}
/* ===============================================================================
|
|   関数名	SetMovePosition
|       処理内容  プレイヤーの位置を設定する
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Player::SetMovePosition(){
	char_pos.x += char_spd.x ;
	char_pos.y += char_spd.y ;
	char_pos.z += char_spd.z ;
}

/* ===============================================================================
|
|   関数名	PlayAction
|       処理内容  プレイヤーの向きをセットする
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Player::PlayAction(){

	//===========================================================
	//		アニメーション再生
	//===========================================================
	anim_playtime += anim_spd ;
	if ( anim_totaltime < anim_playtime ){
		if ( animflg != pLose_S )
			anim_playtime = 0.0f ;
		if ( animflg == pAttack || (animflg == pDown && invi_flg != 1) || animflg == pPress )
			animflg = 0 ;
	}

//	printf( "%d" ,animflg ) ;
	//=========================================================================
	//			アニメーション選択
	//=========================================================================
	if ( animbackup != animflg ){
		animbackup = animflg ;
		switch( animflg ){
			case pStand :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_nutral) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_spd = 1.0f ;
				break ;

			case pRun :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_run) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_spd = 5.0f ;
				break ;

			case pAttack :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_atk) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_spd = 3.0f ;
				anim_playtime = 0.0f ;
				break ;

			case pDown :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_down) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_spd = 2.5f ;
				anim_playtime = 0.0f ;
				break ;

			case pPress :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_press) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_playtime = 0.0f ;
				anim_spd = 1.0f ;
				break ;

			case pWin :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_win) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_playtime = 0.0f ;
				anim_spd = 1.0f ;
				break ;

			case pLose_S :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[0]) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_playtime = 0.0f ;
				anim_spd = 1.0f ;
				break ;

			case pLose :
				MV1DetachAnim(player_model,tmp_attachidx) ;									// --- アニメーションをデタッチ
				tmp_attachidx = MV1AttachAnim(player_model,0,anim_lose[1]) ;					// --- モデルにアタッチ
				anim_totaltime = MV1GetAttachAnimTotalTime(player_model,tmp_attachidx) ;	// --- アニメーション総時間取得
				anim_playtime = 0.0f ;
				anim_spd = 1.0f ;
				break ;
		}
	}
}

/* ===============================================================================
|
|   関数名	p_Init
|       処理内容  モデルハンドルの格納や初期座標のセット
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Player::Respown_Init( int p_No )
{
	// --- 座標 ---------------------------------------------------------------
	switch ( p_No ){
	case PLAYER1 :
			char_pos =  VGet( 300.0f , 300.0f , -250.0f ) ;		// プレイヤー1の初期座標
			char_rot = eDown ;
			char_spd.y = 0.0f ;
			invi_flg = 2 ;
			p_score[1] += 1 ;
			break ;

		case PLAYER2 :
			char_pos =  VGet( 500.0f , 300.0f , -250.0f ) ;		// プレイヤー2の初期座標
			char_rot = eDown ;
			char_spd.y = 0.0f ;
			invi_flg = 2 ;
			p_score[0] += 1 ;
			break ;
	}
}