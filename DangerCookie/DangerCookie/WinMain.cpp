/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: WinMain.cpp
	NAME	: 

+ ------ Explanation of file --------------------------------------------------------------------------
       
	メイン関数

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

#include "Common.h"
#include "time.h"

int WINAPI WinMain(HINSTANCE hI,HINSTANCE hP,LPSTR lpC,int nC){

	// カメラ位置
	cpos = VGet( 400.0f , 700.0f , -1000.0f ) ;
	// カメラ注視点
	ctgt = VGet( 400.0f , 400.0f , -150.0f ) ;

	SetOutApplicationLogValidFlag( FALSE ) ;

// ウインドウモードの切り替え
//	ChangeWindowMode(TRUE) ;		// --- Windowモード
	ChangeWindowMode(FALSE) ;		// --- フルスクリーンモード

	// ウインドウサイズの変更
//	SetGraphMode(900,600,32) ;
	SetGraphMode(1920,1080,32) ;

	// DXライブラリの初期化
	if(DxLib_Init() == -1){
		return -1 ;
	}

	SetDrawScreen(DX_SCREEN_BACK) ;

	SetCameraPositionAndTargetAndUpVec(cpos,ctgt,VGet(0.0f,0.0f,1.0f)) ;

	// ライティングの計算をしないように設定を変更
    SetUseLighting( TRUE ) ;

	// rand()バラバラにする
	srand((unsigned)time(NULL)) ;

	// モデルの初期セット
	Draw_Init_Model() ;
	// タイマーの初期セット
	timer.T_Init() ;
	// キーの初期セット
	KeyStateInit() ;

	// --- sceneの変更の初期化 
	sceneSelect = TITLE_INIT ;
//	sceneSelect = GAME_INIT ;

	 // Y軸のマイナス方向のディレクショナルライトに変更
    //ChangeLightTypeDir( VGet( 0.0f, -1.0f, 0.0f  ) ) ;

	while(ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0)
	{
		GetKeyState() ;

		ClearDrawScreen() ;

		DrawBox(0,0,1200,800,GetColor(100,5,10),true) ; //最後の引数をfalseにすると塗りつぶし無し

		switch( sceneSelect )
		{
			// --- タイトル --- //
			case TITLE_INIT :
				TitleScene_Init() ;

			case TITLE_MODE :
				TitleScene() ;
				break ;

			//  --- キャラ選択 --- //
			case CSELECT_INIT :
				C_Select_Init() ;

			case CSELECT_MODE :
				C_Select() ;
				break ;

			// --- ゲームプレイ --- //
			case GAME_INIT :
				GameScene_Init() ;

			case GAME_MODE :
				GameScene() ;
				break ;

			// --- 結果発表 --- //
			case RESULT_INIT :
				ResultScene_Init() ;
				break ;
			case RESULT_MODE :
				ResultScene() ;
				break ;
		}
		
//		printf( "scene = %d\n " , sceneSelect ) ;
		ScreenFlip() ;
	}

	DxLib_End() ;
	return 0 ;
}

/* -[EOF]- */