/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: Player.h

+ ------ Explanation of file --------------------------------------------------------------------------
       
	プレイヤーの関連関数を集めたクラス

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

class Player{
	public :
		Player() ;
		~Player() ;
		void p_Init( int ) ;
		void SetMovePosition() ;
		void GameStartSpin( int, int ) ;
		void PlayAction() ;
		void Respown_Init( int ) ;

		// キャラ格納
		int		p_Model ;			// --- キャラ情報格納変数
		int		player_model ;		// --- キャラのモデル格納ハンドル
		int		rootflm	;			// --- キャラのルートフレーム（骨組み）の位置を格納
		
		int		p_flg ;
		int		sp_flg ;
		int		sp_Model[3] ;			// --- キャラ情報格納変数
		int     pSelect_Model[3] ;
		VECTOR  select_C_pos  ;

		// キャラクター詳細
		VECTOR	char_pos ;			// --- キャラクターポジション
		VECTOR	char_spd ;			// --- キャラクタースピード(移動量)
		float	char_rot ;			// --- キャラクターローテーション(8方向)

		// アニメーションの格納
		int     anim_nutral ;
		int     anim_run ;
		int     anim_atk ;
		int		anim_down ;
		int		anim_press ;
		int     anim_win ;
		int     anim_lose[2] ;
		int     animflg	;           // --- アニメーションの選択
		int		tmp_attachidx ;		// --- アニメ総時間取得のための一時変数
		float	anim_playtime ;		// --- アニメーション再生時間
		float	anim_totaltime ;	// --- アニメーション総時間
		float	anim_spd ;			// --- アニメーションスピード

		int		invi_flg ;			// --- 無敵フラグ
		int     invi_count ;
		int		animbackup ;		// --- アニメーション切り替え用バックアップ

		int     p_Landing_flg ;     // --- 着地したか
		bool    ready_check ;

		// --- 勝敗決めのための変数 -----------------------
		int		death_count ;		// --- 落ちた回数をカウント
		int		press_count ;		// --- 壁に潰された回数のカウント
		int		attack_count ;		// --- 攻撃して当たった回数のカウント
		BOOL	lastfall_flg ;		// --- 最後に落下したほう

		int		wk[4] ;				// --- 予備配列 int[4]
} ;

