/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: HitCheck.cpp

+ ------ Explanation of file --------------------------------------------------------------------------
       
	ヒットチェック  関数名はPlayerToWall().... 

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

#include "Common.h"
#include <math.h>

#define STAGECOLLOBJ_MAXNUM			256
#define PLAYER_ENUM_DEFAULT_SIZE	400.0f		// 周囲のポリゴン検出に使用する球の初期サイズ
#define PLAYER_MAX_HITCOLL			248
#define PLAYER_HIT_HEIGHT			200.0f		// 当たり判定カプセルの高さ
#define PLAYER_HIT_WIDTH			70.0f		// 当たり判定カプセルの幅
#define PLAYER_HIT_TRYNUM			16			// 壁押し出し処理の最大試行回数
#define PLAYER_HIT_SLIDE_LENGTH		5.0f		// 一度の壁押し出し処理でスライドさせる距離


/* ===============================================================================
|
|   関数名	PlayerToWall_Up
|       処理内容  プレイヤーと壁の下の面の判定
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */	
void PlayerToWall_Up( int p_No, int M_Game_Press_SE )
{
	MV1_COLL_RESULT_POLY_DIM HitDim[STAGECOLLOBJ_MAXNUM] ;		// プレイヤーの周囲にあるポリゴンを検出した結果が代入される当たり判定結果構造体
	MV1_COLL_RESULT_POLY *Kabe[ PLAYER_MAX_HITCOLL ] ;			// 壁ポリゴンと判断されたポリゴンの構造体のアドレスを保存しておくためのポインタ配列
	MV1_COLL_RESULT_POLY *Poly ;								// ポリゴンの構造体にアクセスするために使用するポインタ
	int KabeNum ;			// 壁ポリゴンと判断されたポリゴンの数
	int HitDimNum ;			// HitDim の有効な配列要素数
	int HitFlag ;			// ポリゴンに当たったかどうかを記憶しておくのに使う変数( ０:当たっていない  １:当たった )
	VECTOR OldPos ;			// 移動前の座標
	VECTOR NowPos ;			// 移動後の座標
	int Vdirec ;

	Vdirec = 0 ;
	for ( int i = 0 ; i < cnt ; i++ ){
		MV1SetupCollInfo( collision_Wall[element[i]].w_Model, -1 ) ;
	}

	// 移動前の座標を保存
	OldPos = m_Player[p_No].char_pos ;

	// 移動後の座標を算出
	NowPos = VAdd( m_Player[p_No].char_pos, m_Player[p_No].char_spd ) ;

	// プレイヤーの周囲にあるコリジョンオブジェクトのポリゴンも取得する
	for( int o = 0 ; o < MAX_WALL ; o++ )
	{
		HitDim[o] = MV1CollCheck_Capsule( collision_Wall[element[o]].w_Model,
										  -1,
										  VGet(NowPos.x, NowPos.y + 80.0f, NowPos.z),
										  VGet(NowPos.x, NowPos.y + PLAYER_HEIGHT - 50.0f, NowPos.z),
										  25.0f
										) ;
	}

	// HitDim の有効な数はコリジョンオブジェクトの数
	HitDimNum = MAX_WALL ;

	KabeNum = 0 ;

	// 検出されたポリゴンの数だけ繰り返し
	for( int j = 0 ; j < HitDimNum ; j++ )
	{
		for( int p = 0 ; p < HitDim[ j ].HitNum ; p++ )
		{
			// ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
			if( KabeNum < PLAYER_MAX_HITCOLL )
			{
				// ポリゴンの構造体のアドレスを壁ポリゴンポインタ配列に保存する
				Kabe[KabeNum] = &HitDim[ j ].Dim[ p ] ;

				// 壁ポリゴンの数を加算する
				KabeNum++ ;
			}
		}
	}

	// 壁ポリゴンとの当たり判定処理
	if( KabeNum != 0 )
	{
		// 壁に当たったかどうかのフラグは初期状態では「当たっていない」にしておく
		HitFlag = 0 ;

		// 壁ポリゴンの数だけ繰り返し
		for( int o = 0 ; o < KabeNum ; o++ )
		{
			// i番目の壁ポリゴンのアドレスを壁ポリゴンポインタ配列から取得
			Poly = Kabe[o] ;

			// ポリゴンに当たっていたら当たったフラグを立てた上でループから抜ける
			if( HitCheck_Capsule_Triangle( NowPos, VAdd( NowPos, VGet( NowPos.x, NowPos.y + PLAYER_HEIGHT, NowPos.z ) ), PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == TRUE )
			{
				HitFlag = 1 ;
				break ;
			}
		}

		// 壁に当たっていたらスピードを0にする
		if( HitFlag == 1 )
		{
			if ( m_Player[p_No].animflg != pPress ){
				m_Player[p_No].press_count++ ;
				m_Player[p_No].animflg = pPress ;
				ChangeVolumeSoundMem( 255, M_Game_Press_SE ) ;
				PlaySoundMem( M_Game_Press_SE , DX_PLAYTYPE_BACK, TRUE ) ;
			}
			m_Player[p_No].char_spd.x = 0.0f ;
			m_Player[p_No].char_spd.y = 0.0f ;
			m_Player[p_No].char_spd.z = 0.0f ;
//			printf( "ヒット" ) ;
		}
	}
	for( int o = 0 ; o < HitDimNum ; o++ )
	{
		MV1CollResultPolyDimTerminate( HitDim[o] ) ;
	}

	// 新しい座標を保存する
//		m_Player[i].char_pos = NowPos[i] ;

	//// プレイヤーのモデルの座標を更新する
	//MV1SetPosition( pl.ModelHandle, pl.Position ) ;

}


/* ===============================================================================
|
|   関数名	PlayerToWall_Side
|       処理内容  プレイヤーと壁の側面の当たり判定
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */	
void PlayerToWall_Side( int p_No )
{
	MV1_COLL_RESULT_POLY_DIM HitDim[STAGECOLLOBJ_MAXNUM] ;		// プレイヤーの周囲にあるポリゴンを検出した結果が代入される当たり判定結果構造体
	MV1_COLL_RESULT_POLY *Kabe[ PLAYER_MAX_HITCOLL ] ;			// 壁ポリゴンと判断されたポリゴンの構造体のアドレスを保存しておくためのポインタ配列
	MV1_COLL_RESULT_POLY *Poly ;								// ポリゴンの構造体にアクセスするために使用するポインタ
	int KabeNum ;			// 壁ポリゴンと判断されたポリゴンの数
	int HitDimNum ;			// HitDim の有効な配列要素数
	int HitFlag ;			// ポリゴンに当たったかどうかを記憶しておくのに使う変数( ０:当たっていない  １:当たった )
	VECTOR OldPos ;			// 移動前の座標
	VECTOR NowPos ;			// 移動後の座標
	int Vdirec ;

	Vdirec = 0 ;
	for ( int i = 0 ; i < cnt ; i++ ){
		MV1SetupCollInfo( collision_Wall[element[i]].w_Model, -1 ) ;
	}

	// 移動前の座標を保存
	OldPos = m_Player[p_No].char_pos ;

	// 移動後の座標を算出
	NowPos = VAdd( m_Player[p_No].char_pos, m_Player[p_No].char_spd ) ;

	// プレイヤーの周囲にあるコリジョンオブジェクトのポリゴンも取得する
	for( int o = 0 ; o < MAX_WALL ; o++ )
	{
		HitDim[o] = MV1CollCheck_Capsule( collision_Wall[element[o]].w_Model,
										  -1,
										  VGet(NowPos.x, NowPos.y + 68.0f, NowPos.z),
										  VGet(NowPos.x, NowPos.y + PLAYER_HEIGHT - 80.0f,NowPos.z),
										  40.0f
										) ;
	}

	// HitDim の有効な数はコリジョンオブジェクトの数
	HitDimNum = MAX_WALL ;

	KabeNum = 0 ;

	// 検出されたポリゴンの数だけ繰り返し
	for( int j = 0 ; j < HitDimNum ; j++ )
	{
		for( int p = 0 ; p < HitDim[ j ].HitNum ; p++ )
		{
			// ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
			if( KabeNum < PLAYER_MAX_HITCOLL )
			{
				// ポリゴンの構造体のアドレスを壁ポリゴンポインタ配列に保存する
				Kabe[KabeNum] = &HitDim[ j ].Dim[ p ] ;

				// 壁ポリゴンの数を加算する
				KabeNum++ ;
			}
		}
	}

	// 壁ポリゴンとの当たり判定処理
	if( KabeNum != 0 )
	{
		// 壁に当たったかどうかのフラグは初期状態では「当たっていない」にしておく
		HitFlag = 0 ;

		// 壁ポリゴンの数だけ繰り返し
		for( int o = 0 ; o < KabeNum ; o++ )
		{
			// i番目の壁ポリゴンのアドレスを壁ポリゴンポインタ配列から取得
			Poly = Kabe[o] ;

			// ポリゴンに当たっていたら当たったフラグを立てた上でループから抜ける
			if( HitCheck_Capsule_Triangle( NowPos, VAdd( NowPos, VGet( NowPos.x, NowPos.y + PLAYER_HEIGHT, NowPos.z ) ), PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == TRUE )
			{
				HitFlag = 1 ;
				break ;
			}
		}

		// 壁に当たっていたらスピードを0にする
		if( HitFlag == 1 )
		{
			m_Player[p_No].char_spd.x = 0.0f ;
			m_Player[p_No].char_spd.y = 0.0f ;
			m_Player[p_No].char_spd.z = 0.0f ;
//			printf( "ヒット" ) ;
		}
	}
	for( int o = 0 ; o < HitDimNum ; o++ )
	{
		MV1CollResultPolyDimTerminate( HitDim[o] ) ;
	}

	// 新しい座標を保存する
//		m_Player[i].char_pos = NowPos[i] ;

	//// プレイヤーのモデルの座標を更新する
	//MV1SetPosition( pl.ModelHandle, pl.Position ) ;

}


/* ===============================================================================
|
|   関数名	PlayerToPlayer
|       処理内容  プレイヤーとプレイヤーの当たり判定
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void PlayerToPlayer()
{
	VECTOR p_pos1 ;		// --- 現ポジションにスピードを足したものの入れ物
	VECTOR p_pos2 ;		// --- 現ポジションにスピードを足したものの入れ物

	// --- 現ポジションにスピードを足したもの
	p_pos1 = VAdd( m_Player[PLAYER1].char_pos, m_Player[PLAYER1].char_spd ) ;
	p_pos2 = VAdd( m_Player[PLAYER2].char_pos, m_Player[PLAYER2].char_spd ) ; 

	// ==========================================
	//		1Pヒットチェック
	// ==========================================
	if ( m_Player[PLAYER1].animflg == pRun ){
		// ==========================================
		//		移動 : 右方向 →→→→→
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x > 0.0f) && (m_Player[PLAYER1].char_spd.z == 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 50, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x + 50, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 15, p_pos1.y, p_pos1.z - 25),
										   VGet(p_pos1.x + 15, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 25),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 2.5f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 2.5f ;		// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 15, p_pos1.y, p_pos1.z + 25),
										   VGet(p_pos1.x + 15, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 25),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   
		}
		
		// ==========================================
		//		移動 : 左方向 ←←←←←
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x < 0.0f) && (m_Player[PLAYER1].char_spd.z == 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 50, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x - 50, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 15, p_pos1.y, p_pos1.z + 25),
										   VGet(p_pos1.x - 15, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 25),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 15, p_pos1.y, p_pos1.z - 25),
										   VGet(p_pos1.x - 15, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 25),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 上方向 ↑↑↑↑↑
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x == 0.0f) && (m_Player[PLAYER1].char_spd.z > 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z + 50),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 50),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 25, p_pos1.y, p_pos1.z + 15),
										   VGet(p_pos1.x + 25, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 15),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 25, p_pos1.y, p_pos1.z + 15),
										   VGet(p_pos1.x - 25, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 15),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 下方向 ↓↓↓↓↓
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x == 0.0f) && (m_Player[PLAYER1].char_spd.z < 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z - 50),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 50),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 25, p_pos1.y, p_pos1.z - 15),
										   VGet(p_pos1.x - 25, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 15),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 25, p_pos1.y, p_pos1.z - 15),
										   VGet(p_pos1.x + 25, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 15),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 右上方向 →↑→↑
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x > 0.0f) && (m_Player[PLAYER1].char_spd.z > 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 35, p_pos1.y, p_pos1.z + 35),
										   VGet(p_pos1.x + 35, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 35),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 30, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x + 30, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 2.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z + 30),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 30),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 2.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}									   
		}

		// ==========================================
		//		移動 : 右下方向 →↓→↓
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x > 0.0f) && (m_Player[PLAYER1].char_spd.z < 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 35, p_pos1.y, p_pos1.z - 35),
										   VGet(p_pos1.x + 35, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 35),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z - 30),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 30),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 2.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 30, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x + 30, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = -2.0f ;
			}									   
		}

		// ==========================================
		//		移動 : 左上方向 ←↑←↑
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x < 0.0f) && (m_Player[PLAYER1].char_spd.z > 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 35, p_pos1.y, p_pos1.z + 35),
										   VGet(p_pos1.x - 35, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 35),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z + 30),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 30),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = -2.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 30, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x - 30, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 2.0f ;
			}									   
		}

		// ==========================================
		//		移動 : 左下方向 ←↑←↑
		// ==========================================
		if ( (m_Player[PLAYER1].char_spd.x < 0.0f) && (m_Player[PLAYER1].char_spd.z < 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 35, p_pos1.y, p_pos1.z - 35),
										   VGet(p_pos1.x - 35, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 35),
										   5,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 30, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x - 30, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = 0.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = -2.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z - 30),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 30),
										   25,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER1].char_spd.x = -2.0f ;
				m_Player[PLAYER1].char_spd.y = 0.0f ;
				m_Player[PLAYER1].char_spd.z = 0.0f ;
			}									   
		}
	}

	// ==========================================
	//		2Pヒットチェック
	// ==========================================
	if ( m_Player[PLAYER2].animflg == pRun ){
		// ==========================================
		//		移動 : 右方向 →→→→→
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x > 0.0f) && (m_Player[PLAYER2].char_spd.z == 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 50, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x + 50, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 15, p_pos2.y, p_pos2.z - 25),
										   VGet(p_pos2.x + 15, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 25),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 15, p_pos2.y, p_pos2.z + 25),
										   VGet(p_pos2.x + 15, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 25),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 左方向 ←←←←←
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x < 0.0f) && (m_Player[PLAYER2].char_spd.z == 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 50, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x - 50, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 15, p_pos2.y, p_pos2.z + 25),
										   VGet(p_pos2.x - 15, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 25),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 15, p_pos2.y, p_pos2.z - 25),
										   VGet(p_pos2.x - 15, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 25),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 上方向 ↑↑↑↑↑
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x == 0.0f) && (m_Player[PLAYER2].char_spd.z > 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z + 50),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 50),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 25, p_pos2.y, p_pos2.z + 15),
										   VGet(p_pos2.x + 25, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 15),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 25, p_pos2.y, p_pos2.z + 15),
										   VGet(p_pos2.x - 25, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 15),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 1.5f ;		// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 下方向 ↓↓↓↓↓
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x == 0.0f) && (m_Player[PLAYER2].char_spd.z < 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z - 50),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 50),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = 0.0f ;		// --- z軸スピード調整
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 25, p_pos2.y, p_pos2.z - 15),
										   VGet(p_pos2.x - 25, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 15),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 1.5f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 25, p_pos2.y, p_pos2.z - 15),
										   VGet(p_pos2.x + 25, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 15),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = -1.5f ;	// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y = 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z = -1.5f ;	// --- z軸スピード調整
			}									   
		}

		// ==========================================
		//		移動 : 右上方向 →↑→↑
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x > 0.0f) && (m_Player[PLAYER2].char_spd.z > 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 35, p_pos2.y, p_pos2.z + 35),
										   VGet(p_pos2.x + 35, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 35),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 30, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x + 30, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 2.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z + 30),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 30),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 2.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}									   
		}

		// ==========================================
		//		移動 : 右下方向 →↓→↓
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x > 0.0f) && (m_Player[PLAYER2].char_spd.z < 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 35, p_pos2.y, p_pos2.z - 35),
										   VGet(p_pos2.x + 35, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 35),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z - 30),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 30),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 2.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 30, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x + 30, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = -2.0f ;
			}									   
		}

		// ==========================================
		//		移動 : 左上方向 ←↑←↑
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x < 0.0f) && (m_Player[PLAYER2].char_spd.z > 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 35, p_pos2.y, p_pos2.z + 35),
										   VGet(p_pos2.x - 35, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 35),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z + 30),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 30),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = -2.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 30, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x - 30, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 2.0f ;
			}									   
		}

		// ==========================================
		//		移動 : 左下方向 ←↑←↑
		// ==========================================
		if ( (m_Player[PLAYER2].char_spd.x < 0.0f) && (m_Player[PLAYER2].char_spd.z < 0.0f) ){
			// --- 自キャラの真ん中に当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 35, p_pos2.y, p_pos2.z - 35),
										   VGet(p_pos2.x - 35, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 35),
										   5,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}

			// --- 自キャラの右寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 30, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x - 30, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = 0.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = -2.0f ;
			}									   

			// --- 自キャラの左寄りに当たった時
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z - 30),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 30),
										   25,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				m_Player[PLAYER2].char_spd.x = -2.0f ;
				m_Player[PLAYER2].char_spd.y = 0.0f ;
				m_Player[PLAYER2].char_spd.z = 0.0f ;
			}									   
		}
	}
}

/* ===============================================================================
|
|   関数名	Player_Attack
|       処理内容  プレイヤーの攻撃当たり判定
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Player_Attack( int M_Game_Attack_SE ){
	VECTOR p_pos1 ;		// --- 現ポジションにスピードを足したものの入れ物
	VECTOR p_pos2 ;		// --- 現ポジションにスピードを足したものの入れ物
	int atk_flg = 0 ;

	// --- 現ポジションにスピードを足したもの
	p_pos1 = VAdd( m_Player[PLAYER1].char_pos, m_Player[PLAYER1].char_spd ) ;
	p_pos2 = VAdd( m_Player[PLAYER2].char_pos, m_Player[PLAYER2].char_spd ) ; 

	// ==========================================
	//		1P攻撃ヒットチェック
	// ==========================================
	if ( (m_Player[PLAYER1].animflg == pAttack) && (m_Player[PLAYER1].anim_totaltime / 2.2 < m_Player[PLAYER1].anim_playtime)  && m_Player[PLAYER2].invi_flg == 0){
		// ------------------------------------------
		//		キャラ : 右攻撃 →→→→→
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == eRight ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 90, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x + 90, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= 10.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= 0.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 左攻撃 ←←←←←
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == eLeft ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 90, p_pos1.y, p_pos1.z),
										   VGet(p_pos1.x - 90, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= -10.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= 0.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 上攻撃 ↑↑↑↑↑
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == eUp ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z + 90),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 90),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= 10.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 下攻撃 ↓↓↓↓↓
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == eDown ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x, p_pos1.y, p_pos1.z - 90),
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 90),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= -10.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 左上攻撃 ←↑←↑←
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 1.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 64, p_pos1.y, p_pos1.z + 64),
										   VGet(p_pos1.x - 64, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 64),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= -7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= 7.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 右上攻撃 →↑→↑→
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 2.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 64, p_pos1.y, p_pos1.z + 64),
										   VGet(p_pos1.x + 64, p_pos1.y + PLAYER_HEIGHT, p_pos1.z + 64),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= 7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= 7.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 左下攻撃 ←↓←↓←
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 0.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x - 64, p_pos1.y, p_pos1.z - 64),
										   VGet(p_pos1.x - 64, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 64),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= -7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= -7.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 右下攻撃 →↓→↓→
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 3.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos1.x + 64, p_pos1.y, p_pos1.z - 64),
										   VGet(p_pos1.x + 64, p_pos1.y + PLAYER_HEIGHT, p_pos1.z - 64),
										   PLAYER_WIDTH / 3,
										   p_pos2,
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER1].attack_count++ ;				// --- 1P攻撃カウント
				m_Player[PLAYER2].char_spd.x	= 7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER2].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER2].char_spd.z	= -7.0f ;		// --- z軸スピード調整
				Atk_1pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER2].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}
	}

	// ==========================================
	//		2P攻撃ヒットチェック
	// ==========================================
	if ( (m_Player[PLAYER2].animflg == pAttack) && (m_Player[PLAYER2].anim_totaltime / 2.2 < m_Player[PLAYER2].anim_playtime) && m_Player[PLAYER1].invi_flg == 0 ){
		// ------------------------------------------
		//		キャラ : 右攻撃 →→→→→
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == eRight ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 90, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x + 90, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= 10.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= 0.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 左攻撃 ←←←←←
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == eLeft ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 90, p_pos2.y, p_pos2.z),
										   VGet(p_pos2.x - 90, p_pos2.y + PLAYER_HEIGHT, p_pos2.z),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= -10.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= 0.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 上攻撃 ↑↑↑↑↑
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == eUp ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z + 90),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 90),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= 10.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 下攻撃 ↓↓↓↓↓
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == eDown ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x, p_pos2.y, p_pos2.z - 90),
										   VGet(p_pos2.x, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 90),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= 0.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= -10.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}
		// ------------------------------------------
		//		キャラ : 左上攻撃 ←↑←↑←
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 1.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 64, p_pos2.y, p_pos2.z + 64),
										   VGet(p_pos2.x - 64, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 64),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= -7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= 7.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 右上攻撃 →↑→↑→
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 2.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 64, p_pos2.y, p_pos2.z + 64),
										   VGet(p_pos2.x + 64, p_pos2.y + PLAYER_HEIGHT, p_pos2.z + 64),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= 7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= 7.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 左下攻撃 ←↓←↓←
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 0.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x - 64, p_pos2.y, p_pos2.z - 64),
										   VGet(p_pos2.x - 64, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 64),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= -7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= -7.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}

		// ------------------------------------------
		//		キャラ : 右下攻撃 →↓→↓→
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 3.5f ){
			if ( HitCheck_Capsule_Capsule( VGet(p_pos2.x + 64, p_pos2.y, p_pos2.z - 64),
										   VGet(p_pos2.x + 64, p_pos2.y + PLAYER_HEIGHT, p_pos2.z - 64),
										   PLAYER_WIDTH / 3,
										   p_pos1,
										   VGet(p_pos1.x, p_pos1.y + PLAYER_HEIGHT, p_pos1.z),
										   PLAYER_WIDTH / 2
										  ) )
			{
				atk_flg = 1 ;
				m_Player[PLAYER2].attack_count++ ;				// --- 2P攻撃カウント
				m_Player[PLAYER1].char_spd.x	= 7.0f ;		// --- x軸スピード調整
				m_Player[PLAYER1].char_spd.y	= 0.0f ;		// --- y軸スピード調整
				m_Player[PLAYER1].char_spd.z	= -7.0f ;		// --- z軸スピード調整
				Atk_2pEfeFlg = true ;							// --- エフェクト描画フラグ
				m_Player[PLAYER1].animflg		= pDown ;		// --- 2Pダウンフラグ
			}
		}
	}
	if ( atk_flg == 1 ){
		atk_flg = 0 ;
		PlaySoundMem( M_Game_Attack_SE, DX_PLAYTYPE_BACK, TRUE ) ;
	}
}

