/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: ActionLoop.cpp

+ ------ Explanation of file --------------------------------------------------------------------------
       
		シーン切り替え

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
#include "Common.h"

int T_Start_SE ;			// --- タイトルスタート効果音
int T_BGM ;					// --- タイトルBGM

int C_Select_SE[2][3] ;		// --- [PlayerNo][0] : 決定	  [PlayerNo][1] : キャンセル   [PlayerNo][2] : セレクト
int C_Ready_SE ;			// --- Are You Readyサウンド
int C_Rot_SE ;				// --- 決定後スピン効果音
int C_Select_BGM ;			// --- キャラセレクトBGM

int M_Game_Count_SE ;		// --- 3.2.1カウントダウン効果音
int M_Game_BGM ;			// --- ゲーム中効果音
int M_Game_Attack_SE ;		// --- 攻撃効果音
int M_Game_Fall_SE ;		// --- 画面外落下効果音
int M_Game_Wall_SE ;		// --- 壁の倒れる効果音
int M_Game_Walk_SE[2] ;		// --- プレイヤーの足音
int M_Game_Press_SE ;		// --- プレイヤーの潰れ効果音
int M_Game_Finish_SE ;		// --- フィニッシュ効果音
int M_Game_To_Result_SE ;

int	Result_SE[4] ;			// --- [0]ドラムロール効果音	[1]歓声サウンド		[2]幕引き	[3]エフェクト
int	Result_BGM ;			// --- リザルトBGM

/************************************************************

			              タイトル				
										
 ************************************************************/
/* ===============================================================================
|
|   関数名	TitleScene_Init
|       処理内容	タイトル画面 初期化
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void TitleScene_Init()
{
	T_Start_SE	= LoadSoundMem( "..\\Data\\Sound\\Title\\TitleStart.mp3" ) ;	// --- タイトルスタート効果音
	T_BGM		= LoadSoundMem( "..\\Data\\Sound\\Title\\T_BGM.mp3" ) ;			// --- タイトルBGM
	sceneFlg = 0 ;
	DrawInit() ;					// --- タイトル初期セット
	sceneSelect = TITLE_MODE ;		// --- モード切替
}
/* ===============================================================================
|
|   関数名	TitleScene
|       処理内容	タイトル画面
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void TitleScene()
{
	static int title_BGM = 0 ;

	if ( title_BGM == 0 ){
		title_BGM = 1 ;
		PlaySoundMem( T_BGM, DX_PLAYTYPE_LOOP, TRUE ) ;
	}

	DrawTitle() ;
	timer.t_time++ ;
	for ( int i = 0 ; i < PLAYER_MODEL ; i++ ){
		t_Player[i].anim_playtime += t_Player[i].anim_spd ;
		if ( t_Player[i].anim_totaltime < t_Player[i].anim_playtime ){
			t_Player[i].anim_playtime = 0.0f ;
		}
	}


	if ( ( g_padstate[PLAYER1].input & PAD_INPUT_START ||
		g_padstate[PLAYER2].input & PAD_INPUT_START ) && sceneFlg == 0 ){
			PlaySoundMem( T_Start_SE, DX_PLAYTYPE_BACK, TRUE ) ;
			sceneFlg = 1 ;
			timer.t_time = 0 ;
	}

	if( sceneFlg == 1 && timer.t_time >= 250 )
	{
		StopSoundMem( T_BGM ) ;					// --- BGMストップ
		sceneSelect = CSELECT_INIT ;			// --- モード切替
	}

}

/************************************************************

			            キャラ選択				
										
 ************************************************************/
/* ===============================================================================
|
|   関数名	C_Select_Init
|       処理内容	キャラ選択　初期化
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void C_Select_Init( void )
{
	for ( int i= 0 ; i < MAX_PLAYER ; i++ ){
		C_Select_SE[i][0] = LoadSoundMem( "..\\Data\\Sound\\C_Select\\Chara_OK.wav" ) ;		// --- キャラ決定効果音
		C_Select_SE[i][1] = LoadSoundMem( "..\\Data\\Sound\\C_Select\\Chara_Cancel.wav" ) ;	// --- キャラキャンセル効果音
		C_Select_SE[i][2] = LoadSoundMem( "..\\Data\\Sound\\C_Select\\C_Select.mp3" ) ;		// --- キャラチェンジ効果音
	}

	C_Ready_SE		= LoadSoundMem( "..\\Data\\Sound\\C_Select\\AreYouReady.wav" ) ;		// --- Are You Ready効果音
	C_Rot_SE		= LoadSoundMem( "..\\Data\\Sound\\C_Select\\tobi1.mp3" ) ;				// --- スピン効果音
	C_Select_BGM	= LoadSoundMem( "..\\Data\\Sound\\C_Select\\雲にのりたい.mp3" ) ;		// --- キャラセレクトBGM

	Draw_Cselect_Init() ;				// --- セレクト初期セット

	sceneFlg = 0 ;
	sceneSelect = CSELECT_MODE ;		// --- モード切替
}
/* ===============================================================================
|
|   関数名	C_Select
|       処理内容	キャラ選択
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void C_Select( void )
{
	Draw_Cselect() ;
	static int ready_flg = 0 ;

	{
		static BOOL C_Sele_flg = false ;
		if ( C_Sele_flg == false ){
			C_Sele_flg = true ;
			ChangeVolumeSoundMem( 150, C_Select_BGM ) ;
			PlaySoundMem( C_Select_BGM, DX_PLAYTYPE_LOOP, TRUE ) ;
		}
	}
	// --- キャラクター選択 --- //
	/* m_Player[i].p_flgでキャラクターを判別 */
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){

		// --- 赤色のキャラクターが描画されてるとき
		if( m_Player[i].p_flg == PLAYER1 ){
			if ( m_Player[i].sp_flg == 0 ){
				if( g_padstate[i].input & PAD_INPUT_LEFT && m_Player[i].ready_check == false){
					StopSoundMem( C_Select_SE[i][2] ) ;
					PlaySoundMem( C_Select_SE[i][2], DX_PLAYTYPE_BACK, TRUE ) ;
					m_Player[i].p_flg = PLAYER3 ;
					m_Player[i].sp_flg = 1 ;
				}
				if( g_padstate[i].input & PAD_INPUT_RIGHT && m_Player[i].ready_check == false){
					StopSoundMem( C_Select_SE[i][2] ) ;
					PlaySoundMem( C_Select_SE[i][2], DX_PLAYTYPE_BACK, TRUE ) ;
					m_Player[i].p_flg = PLAYER2 ;
					m_Player[i].sp_flg = 1 ;
				}
			}
			if ( g_padstate[i].input == FALSE ) {
				m_Player[i].sp_flg = 0 ;
			}
		}

		// --- 青色のキャラクターが描画されてるとき
		if( m_Player[i].p_flg == PLAYER2 ){
			if ( m_Player[i].sp_flg == 0 ){
				if( g_padstate[i].input & PAD_INPUT_LEFT && m_Player[i].ready_check == false){
					StopSoundMem( C_Select_SE[i][2] ) ;
					PlaySoundMem( C_Select_SE[i][2], DX_PLAYTYPE_BACK, TRUE ) ;
					m_Player[i].p_flg = PLAYER1 ;
					m_Player[i].sp_flg = 1 ;
			
				}
				if( g_padstate[i].input & PAD_INPUT_RIGHT && m_Player[i].ready_check == false){
					StopSoundMem( C_Select_SE[i][2] ) ;
					PlaySoundMem( C_Select_SE[i][2], DX_PLAYTYPE_BACK, TRUE ) ;
					m_Player[i].p_flg = PLAYER3 ;
					m_Player[i].sp_flg = 1 ;
			
				}
			}
			if ( g_padstate[i].input == FALSE ) {
				m_Player[i].sp_flg = 0 ;
			}
		}

		// --- 緑色のキャラクターが描画されてるとき
		if( m_Player[i].p_flg == PLAYER3 ){
			if ( m_Player[i].sp_flg == 0 ){
				if( g_padstate[i].input & PAD_INPUT_LEFT && m_Player[i].ready_check == false){
					StopSoundMem( C_Select_SE[i][2] ) ;
					PlaySoundMem( C_Select_SE[i][2], DX_PLAYTYPE_BACK, TRUE ) ;
					m_Player[i].p_flg = PLAYER2 ;
					m_Player[i].sp_flg = 1 ;
			
				}
				if( g_padstate[i].input & PAD_INPUT_RIGHT && m_Player[i].ready_check == false){
					StopSoundMem( C_Select_SE[i][2] ) ;
					PlaySoundMem( C_Select_SE[i][2], DX_PLAYTYPE_BACK, TRUE ) ;
					m_Player[i].p_flg = PLAYER1 ;
					m_Player[i].sp_flg = 1 ;
			
				}
			}
			if ( g_padstate[i].input == FALSE ) {
				m_Player[i].sp_flg = 0 ;
			}
		}
	}

	if( m_Player[PLAYER1].ready_check == true && 
		m_Player[PLAYER2].ready_check == true ){
		if ( ready_flg == 0 ){
			ready_flg = 1 ;
			PlaySoundMem( C_Ready_SE, DX_PLAYTYPE_BACK, TRUE ) ;
		}
		if ( CheckSoundMem( C_Ready_SE ) != -1 ) {
			if ( ready_flg == 1 ){
				ready_flg = 2 ;
				PlaySoundMem( C_Rot_SE, DX_PLAYTYPE_BACK, TRUE ) ;
			}
			timer.Title_To_GameTimer() ;
		}
	}

	if ( (g_padstate[PLAYER1].input & PAD_INPUT_B) && (CheckSoundMem( C_Select_SE[0][0] ) != 1) && (m_Player[PLAYER1].ready_check == false) ){
		PlaySoundMem( C_Select_SE[0][0], DX_PLAYTYPE_BACK, TRUE ) ;
		m_Player[PLAYER1].ready_check = true ;
	}
	
	if ( (g_padstate[PLAYER2].input & PAD_INPUT_B) && (CheckSoundMem( C_Select_SE[1][0] ) != 1) && (m_Player[PLAYER2].ready_check == false) ){
		PlaySoundMem( C_Select_SE[1][0], DX_PLAYTYPE_BACK, TRUE ) ;
		m_Player[PLAYER2].ready_check = true ;
	}

	if ( (g_padstate[PLAYER1].input & PAD_INPUT_C) && (CheckSoundMem( C_Select_SE[0][1] ) != 1) && (m_Player[PLAYER1].ready_check == true) && (m_Player[PLAYER2].ready_check == false) ){
		PlaySoundMem( C_Select_SE[0][1], DX_PLAYTYPE_BACK, TRUE ) ;
		m_Player[PLAYER1].ready_check = false ;
	}

	if ( (g_padstate[PLAYER2].input & PAD_INPUT_C) && (CheckSoundMem( C_Select_SE[1][1] ) != 1) && (m_Player[PLAYER2].ready_check == true) && (m_Player[PLAYER1].ready_check == false) ){
		PlaySoundMem( C_Select_SE[1][1], DX_PLAYTYPE_BACK, TRUE ) ;
		m_Player[PLAYER2].ready_check = false ;
	}

	// --- フラグで管理
	if( m_Player[PLAYER1].ready_check == true && 
		m_Player[PLAYER2].ready_check == true &&
		m_Player[PLAYER1].select_C_pos.y > 1500 ){
		StopSoundMem( C_Select_BGM ) ;		// --- BGMストップ
		sceneSelect = GAME_INIT ;			// --- モード切替
	}


}

/************************************************************

			            ゲーム画面			
										
 ************************************************************/
/* ===============================================================================
|
|   関数名	GameScene_Init
|       処理内容	ゲーム画面 初期化
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void GameScene_Init()
{
//	SetWallType() ;
	CheeringInit() ;

	M_Game_BGM			= LoadSoundMem( "..\\Data\\Sound\\M_Game\\M_Game_BGM.mp3" ) ;		// --- ゲームBGM
	M_Game_Count_SE		= LoadSoundMem( "..\\Data\\Sound\\M_Game\\Countdown03-5.mp3" ) ;	// --- 3.2.1のカウントダウン効果音
	M_Game_Attack_SE	= LoadSoundMem( "..\\Data\\Sound\\M_Game\\Attack.mp3" ) ;			// --- 攻撃効果音
	M_Game_Fall_SE		= LoadSoundMem( "..\\Data\\Sound\\M_Game\\Fall1.wav" ) ;			// --- ステージ外落下効果音
	M_Game_Walk_SE[0]	= LoadSoundMem( "..\\Data\\Sound\\M_Game\\Walk.mp3" ) ;				// --- キャラの足音
	M_Game_Walk_SE[1]	= LoadSoundMem( "..\\Data\\Sound\\M_Game\\Walk.mp3" ) ;				// --- キャラの足音
	M_Game_Wall_SE		= LoadSoundMem( "..\\Data\\Sound\\M_Game\\wall_down01.mp3" ) ;		// --- 壁の倒れる効果音
	M_Game_Press_SE		= LoadSoundMem( "..\\Data\\Sound\\M_Game\\Press01.wav" ) ;			// --- キャラの潰れ効果音
	M_Game_Finish_SE	= LoadSoundMem( "..\\Data\\Sound\\M_Game\\finish1.mp3" ) ;			// --- フィニッシュ効果音

	M_Game_To_Result_SE = LoadSoundMem( "..\\Data\\Sound\\M_Game\\nagarebosi_1.wav" ) ;		// --- 幕引き効果音

	// --- プレイヤー関連 -----------------------------------------------------------------------------------------
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		m_Player[i].p_Init( i ) ;	// --- プレイヤー初期セット
	}

	sceneSelect = GAME_MODE  ;		// --- モード切替
}

/* ===============================================================================
|
|   関数名	GameScene
|       処理内容	ゲーム画面
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void GameScene()
{
	static BOOL bgm_flg = false ;

	if ( bgm_flg == false ){
		bgm_flg = true ;
		ChangeVolumeSoundMem( 150, M_Game_BGM ) ;
		PlaySoundMem( M_Game_BGM, DX_PLAYTYPE_LOOP, FALSE ) ;
	}

	/* ここに演出を入れます */

		/* ここに演出を入れます */

	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		m_Player[i].GameStartSpin( M_Game_Count_SE, i ) ;
	}

	if( m_Player[PLAYER1].p_Landing_flg == 1 && 
		m_Player[PLAYER2].p_Landing_flg == 1 && startFlg == false ){

			static VECTOR camera_spd = VGet( 0.0f , 6.0f , -8.0f ) ;
			static VECTOR camera_pos = VGet( 400.0f , 400.0f , -600.0f ) ;
			static BOOL sound_f = false ;

			if ( sound_f == false ){
				sound_f = true ;
				PlaySoundMem( M_Game_Count_SE, DX_PLAYTYPE_BACK, TRUE ) ;
			}
			camera_pos.y += camera_spd.y ;
			camera_pos.z += camera_spd.z ;
			cpos = VGet( camera_pos.x , camera_pos.y , camera_pos.z ) ;
			ctgt = VGet( 400.0f , 400.0f , -150.0f ) ;
			SetCameraPositionAndTargetAndUpVec(cpos,ctgt,VGet(0.0f,1.0f,0.0f))  ;

			if( camera_pos.y >= 700.0f)
			{	
				// カメラ位置
				cpos = VGet( 400.0f , 700.0f , -1000.0f ) ;
				// カメラ注視点
				ctgt = VGet( 400.0f , 400.0f , -150.0f ) ;
				SetCameraPositionAndTargetAndUpVec(cpos,ctgt,VGet(0.0f,1.0f,0.0f))  ;

				timer.Direct_Timer() ;
//				printf( "timer = %d\n" , timer.d_time) ;

				if(timer.d_time >= 15)
				{
					startFlg = true  ;
				}
			}
		}
// --------------------------------------------------------------


	if( startFlg == true)
	{
		// --- ゲームプレイ中の時間計測
		timer.GameTimer() ;

		// --- マグマのアニメーション
		Maguma_Animation() ;	

	if ( timer.tk_time > 0 ){
		for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
				//  復活した際の初期配置
				if ( m_Player[i].char_pos.y < -100 ){
					m_Player[i].Respown_Init( i ) ;
				}
				if( m_Player[i].animflg != pPress ){
					// キー情報の取得 落下中は取得しない
					if( m_Player[i].animflg != pDown && m_Player[i].invi_flg != 1 ){
						PlayerKey( i ) ;		
					}
				}
				m_Player[i].PlayAction() ;
				if ( m_Player[i].animflg == pRun ){
					if ( CheckSoundMem( M_Game_Walk_SE[i] ) != 1 ){
						ChangeVolumeSoundMem( 100, M_Game_Walk_SE[i] ) ;
						Set3DPositionSoundMem( m_Player[i].char_pos, M_Game_Walk_SE[i] ) ;
						PlaySoundMem( M_Game_Walk_SE[i], DX_PLAYTYPE_BACK, TRUE ) ;
					}
				}
			}
	
			// 二枚目の壁の出現タイミング（最初）
			for ( int i = 0 ; i < cnt ; i++ ){
//				if(collapse_Wall[element[i]].w_rot < 87 && rotflg == FALSE){
//					rotflg = TRUE ;
//				}
				if ( collapse_Wall[element[i]].w_pos.y < 240 ){
					// 壁の種類・位置・数をランダム取得
					SetWallType() ;
				}
				collapse_Wall[element[i]].SetMoveRotation(M_Game_Wall_SE) ;
				collision_Wall[element[i]].SetMoveRotation(M_Game_Wall_SE) ;
				Shadow_Move( i ) ;
			}
		}else{
			for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
				m_Player[i].char_spd = VGet(0.0f, 0.0f, 0.0f) ;
				MV1SetPosition( m_Player[i].p_Model, m_Player[i].char_spd ) ;
			}
		}

		PlayerFallDown( ) ;			// --- プレイヤーの落下
		{
			static BOOL fall_flg[2] = {false,false} ;
			for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
				if ( fall_flg[i] == false && m_Player[i].invi_flg == 1 ){
					fall_flg[i] = true ;
					PlaySoundMem( M_Game_Fall_SE, DX_PLAYTYPE_BACK, TRUE ) ;
				}
				if ( m_Player[i].invi_flg != 1 )
					fall_flg[i] = false ;
			}
		}
		PlayerToPlayer( ) ;			// --- プレイヤーのヒットチェック
		Player_Attack( M_Game_Attack_SE ) ;	

		for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
			if( m_Player[i].invi_flg == 0 )
			{
				// --- 壁ヒットチェック
				for ( int o = 0 ; o < cnt ; o++ ){
					if ( timer.g_time < 20 ){
						if ( collapse_Wall[element[o]].tsw_spd >= 0.017f )
							PlayerToWall_Up( i, M_Game_Press_SE ) ;
					}
					if ( timer.g_time >= 20 && timer.g_time < 40 ){
						if ( collapse_Wall[element[o]].tsw_spd >= 0.011f )
							PlayerToWall_Up( i, M_Game_Press_SE ) ;
					}
					if ( timer.g_time >= 40 ){
						if ( collapse_Wall[element[o]].tsw_spd >= 0.009f )
							PlayerToWall_Up( i, M_Game_Press_SE ) ;
					}
				}
				PlayerToWall_Side( i ) ;
			}
		}
	}

	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		for ( int y = 0 ; y < 4 ; y++ )
			f_Player[i][y].PlayAction() ;
		o_Player[i].PlayAction() ;
	}
	DrawGameModel() ;				// --- モデルの描画

	{
		static BOOL finish_flg = false ;

		if (timer.tk_time <= 0 && finish_flg == false){
			finish_flg = true ;
			PlaySoundMem( M_Game_Finish_SE, DX_PLAYTYPE_BACK, TRUE ) ;
		}
	}

	// ---　幕のSE
	{
		static bool str_result_flg = false ;

		if (maku_pos_x < 1920 && str_result_flg != true){
			str_result_flg = true ;
			ChangeVolumeSoundMem( 120, M_Game_To_Result_SE ) ;
			PlaySoundMem( M_Game_To_Result_SE, DX_PLAYTYPE_BACK, TRUE ) ;
		}
	}

	// デバック用　時短
/*	if(timer.g_time >= 5){
		kuromaku_flg = true ;
		DrawBox(maku_pos_x,0,(maku_pos_x+2520),154,GetColor(   255 , 0 , 0),true) ;				//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+100),154,(maku_pos_x+2520),308,GetColor( 255 ,165 ,0),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+200),308,(maku_pos_x+2520),462,GetColor( 255 ,255 ,0),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+300),462,(maku_pos_x+2520),616,GetColor( 0   ,128 ,0),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+400),616,(maku_pos_x+2520),770,GetColor( 0   ,255 ,255),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+500),770,(maku_pos_x+2520),924,GetColor( 0   ,0   ,255),true) ;		//最後の引数をfalseにすると塗りつぶし無し(青)
		DrawBox((maku_pos_x+600),924,(maku_pos_x+2520),1080,GetColor(128 ,0   ,128),true) ;		//最後の引数をfalseにすると塗りつぶし無し(緑)
		if(maku_pos_x <= -600){
			sceneSelect = RESULT_INIT  ;
			StopSoundMem( M_Game_BGM ) ;
		}
	}
*/
	// --- 黒幕表示と画面は時まで黒幕が進んだらシーン切り替え
	if(timer.tk_time <= -5){
		kuromaku_flg = true ;
		DrawBox(maku_pos_x,0,(maku_pos_x+2520),154,GetColor(   255 , 0 , 0),true) ;				//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+100),154,(maku_pos_x+2520),308,GetColor( 255 ,165 ,0),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+200),308,(maku_pos_x+2520),462,GetColor( 255 ,255 ,0),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+300),462,(maku_pos_x+2520),616,GetColor( 0   ,128 ,0),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+400),616,(maku_pos_x+2520),770,GetColor( 0   ,255 ,255),true) ;		//最後の引数をfalseにすると塗りつぶし無し(赤)
		DrawBox((maku_pos_x+500),770,(maku_pos_x+2520),924,GetColor( 0   ,0   ,255),true) ;		//最後の引数をfalseにすると塗りつぶし無し(青)
		DrawBox((maku_pos_x+600),924,(maku_pos_x+2520),1080,GetColor(128 ,0   ,128),true) ;		//最後の引数をfalseにすると塗りつぶし無し(緑)
		if(maku_pos_x <= -600){
			sceneSelect = RESULT_INIT  ;
			StopSoundMem( M_Game_BGM ) ;
		}
	}

}


/************************************************************

			         リザルト画面			
										
 ************************************************************/
/* ===============================================================================
|
|   関数名	ResultScene_Init
|       処理内容	リザルト画面 初期化
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void ResultScene_Init()
{
	Result_SE[0]	= LoadSoundMem( "..\\Data\\Sound\\Result\\drumroll.wav" ) ;						// --- ドラムロール効果音
	Result_SE[1]	= LoadSoundMem( "..\\Data\\Sound\\Result\\people-performance-cheer1.mp3" ) ;	// --- 歓声サウンド
	Result_SE[2]	= LoadSoundMem( "..\\Data\\Sound\\Result\\nagarebosi_2.wav" ) ;					// --- 幕引きサウンド
	Result_SE[3]	= LoadSoundMem( "..\\Data\\Sound\\Result\\Result_SE.mp3" ) ;					// --- エフェクト

	Result_BGM		= LoadSoundMem( "..\\Data\\Sound\\Result\\さわやかサンシャイン.mp3" ) ;			// --- さわやかサンシャイン

	DrawResult_Init() ;				// --- リザルト初期セット

	sceneSelect = RESULT_MODE ;		// --- モード切替
}

/* ===============================================================================
|
|   関数名	ResultScene
|       処理内容	リザルト画面
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void ResultScene()
{
	// ---　幕のSE
	{
		static BOOL sound_maku_SE = FALSE ;

		if ( sound_maku_SE == FALSE ){
			sound_maku_SE = TRUE ;
			ChangeVolumeSoundMem( 120, Result_SE[2] ) ;
			PlaySoundMem( Result_SE[2], DX_PLAYTYPE_BACK, TRUE ) ;
		}

	}

	for(int i = 0 ; i < MAX_PLAYER ; i++){
		t_Player[i].PlayAction() ;
	}

	if(maku_pos_x <= -2620){
		{
			static BOOL sound_d_SE = false ;
			static BOOL sound_s_SE = false ;

			if ( sound_d_SE == false ){
				sound_d_SE = true ;
				ChangeVolumeSoundMem( 250, Result_SE[0] ) ;
				PlaySoundMem( Result_SE[0], DX_PLAYTYPE_BACK, TRUE ) ;
			}

			if ( CheckSoundMem( Result_SE[0] ) != 1 && sound_s_SE == false ){
				sound_s_SE = true ;
				PlaySoundMem( Result_SE[1], DX_PLAYTYPE_BACK, TRUE ) ;
				ChangeVolumeSoundMem( 180, Result_BGM ) ;
				PlaySoundMem( Result_BGM, DX_PLAYTYPE_LOOP, TRUE ) ;
			}
		}
		timer.ResultTimer() ;
	}
	if( timer.re_time >= 8 ){
		{
			static BOOL Effect_SE = false ;

			if ( Effect_SE == false ){
				Effect_SE = true ;
				ChangeVolumeSoundMem( 140, Result_SE[3] ) ;
				PlaySoundMem( Result_SE[3], DX_PLAYTYPE_BACK, TRUE ) ;
			}
		}

	}

	DrawResult() ;
}

