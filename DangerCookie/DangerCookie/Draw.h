/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: Draw.h

+ ------ Explanation of file --------------------------------------------------------------------------
       
		モデルの描画、拡大などを担当するクラス

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

void Draw_Init_Model() ;

void Maguma_Animation() ;

void tennsuu( ) ;

void CheeringInit() ;

void DrawGameModel() ;

void DrawInit() ;

void DrawTitle() ;

void Draw_Cselect_Init() ;

void Draw_Cselect() ;

void DrawResult_Init() ;

void DrawResult() ;

void DrawEffect();
