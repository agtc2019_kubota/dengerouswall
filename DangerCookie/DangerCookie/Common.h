
/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: Common.h

+ ------ Explanation of file --------------------------------------------------------------------------
       
	全てのファイルで共通のヘッダーファイル

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

/* -----------------------------------------------------------------------------------------
|
|       DEFINE宣言
|
+ --------------------------------------------------------------------------------------- */
#define MAX_PLAYER		2

#define PLAYER1			0
#define PLAYER2			1
#define PLAYER3			2

#define PLAYER_MODEL    3  

#define PLAYER_HEIGHT	200
#define PLAYER_WIDTH	110

#define MAX_OBJ			7

#define MAX_WALL		5

#define STAGE_LIMIT_RIGHT   1010
#define STAGE_LIMIT_LEFT   -220
#define STAGE_LIMIT_UP      210
#define STAGE_LIMIT_DOWN   -510

#define TITLE_INIT        0
#define TITLE_MODE        1
#define CSELECT_INIT      2
#define CSELECT_MODE      3
#define GAME_INIT         4
#define GAME_MODE         5
#define RESULT_INIT       6
#define RESULT_MODE       7

#define DEBUG           50

#define PLAYER_MODEL_1		"..\\Data\\Player\\M_1P\\mv1\\M_1P.mv1"
#define PLAYER_SELECT_1_S	"..\\Data\\Player\\M_1P\\mqo\\M_1P_Select.mqo"
#define PLAYER_MODEL_2		"..\\Data\\Player\\M_2P\\mv1\\M_2P.mv1"
#define PLAYER_SELECT_2_S	"..\\Data\\Player\\M_2P\\mqo\\M_2P_Select.mqo"
#define PLAYER_MODEL_3		"..\\Data\\Player\\M_3P\\mv1\\M_3P.mv1"
#define PLAYER_SELECT_3_S	"..\\Data\\Player\\M_3P\\mqo\\M_3P_Select.mqo"


/* -----------------------------------------------------------------------------------------
|
|       共通ヘッダー
|
+ --------------------------------------------------------------------------------------- */
#include <DxLib.h>
#include <windows.h>
#include <stdio.h>

#include "ActionLoop.h"
#include "BattleAction.h"
#include "Draw.h"
#include "HitCheck.h"
#include "Player.h"
#include "Wall.h"
#include "Timer.h"

/* -----------------------------------------------------------------------------------------
|
|       型宣言
|
+ --------------------------------------------------------------------------------------- */
enum Direction
{
	eDown,
	eLeft,
	eUp,
	eRight
} ;

enum ActionMode
{
	pStand  ,
	pRun    ,
	pAttack ,
	pDown   ,
	pPress  ,
	pWin    ,
	pLose_S ,
	pLose   ,
	pTotal
} ;

/* -----------------------------------------------------------------------------------------
|
|       構造体宣言
|
+ --------------------------------------------------------------------------------------- */
typedef struct {		// パッド情報を格納する構造体
		int input ;		// 入力状態
		int botton ;    // ボタン

} PAD_STATE ;

extern PAD_STATE g_padstate[MAX_PLAYER] ;

typedef void ( *TBLJP )( void ) ;

/* -----------------------------------------------------------------------------------------
|
|       プロトタイプ宣言
|
+ --------------------------------------------------------------------------------------- */
void PlayerToWall_Up( int, int ) ;
void PlayerToWall_Side( int ) ;


/* -----------------------------------------------------------------------------------------
|
|       外部参照宣言
|
+ --------------------------------------------------------------------------------------- */
extern VECTOR  cpos ;
extern VECTOR  ctgt ;

extern Player	m_Player[MAX_PLAYER] ;
extern Player	t_Player[PLAYER_MODEL] ;
extern Player	f_Player[MAX_PLAYER][4] ;
extern Player	o_Player[MAX_PLAYER] ;
extern Player	title_Flager[3] ;
extern Player	Right_title_Flager[3] ;
extern Wall		collapse_Wall[MAX_WALL] ;
extern Wall		collision_Wall[MAX_WALL] ;		// --- 壁クラスのオブジェクト化
extern Wall		shadow_Wall[MAX_WALL] ;
extern Timer    timer ;

extern int		Model[MAX_OBJ] ;

extern int		cnt ;
extern int		element[] ;
extern int		wposNo[] ;
extern int		check ;
extern bool     rotflg ;

extern int      sceneSelect;
extern int      sceneFlg ;

extern int		score[2][6] ;
extern int		mc[10] ;
extern int		me[10] ;
extern int		md[10] ;
extern int		my[10] ;
extern int		mr[10] ;
extern int      Speed_Grp ;
extern int      TimeHandle ;
extern int		p_score[2] ;
extern float	Transparency ;

extern bool     startFlg ;

extern float    finish_flg ;

extern bool     Atk_1pEfeFlg ;
extern bool     Atk_2pEfeFlg ;

// --- 黒幕用
extern bool		kuromaku_flg ;				// --- 黒幕表示のタイミングに使うフラグ
extern int		maku_pos_x ;				// --- 黒幕のxポジ

/* -----------------------------------------------------------------------------------------
|
|       グローバル変数の外部参照宣言
|
+ --------------------------------------------------------------------------------------- */



/* -[EOF]- */

