/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: BattleAction.cpp

+ ------ Explanation of file --------------------------------------------------------------------------
       
	プレイヤーの移動・攻撃・アニメーション

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
#include "Common.h"

int g_padnum ;			// パッド(プレイヤー)の数

/* ===============================================================================
|
|   関数名	KeyStateInit
|       処理内容  プレイヤーのキー初期セット
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void KeyStateInit()
{
	g_padnum = GetJoypadNum() ;	// --- 接続されているパッドの数を格納

	// --- 接続されたパッドの数だけボタン情報をセット
	for( int i = 0 ; i < g_padnum ; i++)
	{
		g_padstate[i].botton = PAD_INPUT_A ;
	}
}

/* ===============================================================================
|
|   関数名	GetKeyState
|       処理内容  キーの情報を取得する 
|				  常に呼び出すのでWinMainのwhile文の中
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void GetKeyState()
{
	// --- P1,P2のパッドの入力状態の確認
	g_padstate[PLAYER1].input = GetJoypadInputState(DX_INPUT_PAD1) ;
	g_padstate[PLAYER2].input = GetJoypadInputState(DX_INPUT_PAD2) ;

}

/* ===============================================================================
|
|   関数名	PlayerKey
|       処理内容  プレイヤーのキー設定
+ --------------------------------------------------------------------------------
|   IN  --> num        --> 1P,2Pの判別
|						   0は1P 1は2P
|
|		--> way        --> 何が入力されたか? 
|						   移動は複数キーあるのでこれで選択する
|
|   OUT --> result     --> 正常終了の判定 
+ ============================================================================= */
void PlayerKey( int num )
{
	m_Player[num].char_spd.x = 0.0 ;
	m_Player[num].char_spd.y = 0.0 ;
	m_Player[num].char_spd.z = 0.0 ;

	if( m_Player[num].animflg != pAttack )
	{
		m_Player[num].animflg = pStand ;

		if ( g_padstate[num].input & PAD_INPUT_DOWN){
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = eDown ;
			m_Player[num].char_spd.z = -5.0 ; 
		}
		if ( g_padstate[num].input & PAD_INPUT_UP ){
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = eUp ;
			m_Player[num].char_spd.z = 5.0 ; 					
		}
		if ( g_padstate[num].input & PAD_INPUT_LEFT ){
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = eLeft ;
			m_Player[num].char_spd.x = -5.0 ; 					
		}
		if ( g_padstate[num].input & PAD_INPUT_RIGHT ){
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = eRight ;
			m_Player[num].char_spd.x = 5.0 ; 					
		}

		//左上
		if( (g_padstate[num].input & PAD_INPUT_UP) &&  (g_padstate[num].input & PAD_INPUT_LEFT ))
		{
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = 1.5f ;
			m_Player[num].char_spd.x = -2.5 ;
			m_Player[num].char_spd.z =  2.5 ;
		}
		//左下
		if( (g_padstate[num].input & PAD_INPUT_DOWN) &&  (g_padstate[num].input & PAD_INPUT_LEFT ))
		{
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = 0.5f ;
			m_Player[num].char_spd.x = -2.5 ;
			m_Player[num].char_spd.z = -2.5 ;
		}
		//右上
		if( (g_padstate[num].input & PAD_INPUT_UP) &&  (g_padstate[num].input & PAD_INPUT_RIGHT ))
		{
			m_Player[num].animflg = pRun ;
			m_Player[num].char_rot = 2.5f ;
			m_Player[num].char_spd.x =  2.5 ;
			m_Player[num].char_spd.z =  2.5 ;
		}
		//右下
		if( (g_padstate[num].input & PAD_INPUT_DOWN) &&  (g_padstate[num].input & PAD_INPUT_RIGHT ))
		{
			m_Player[num].animflg =  pRun ;
			m_Player[num].char_rot = 3.5f ;
			m_Player[num].char_spd.x =  2.5 ;
			m_Player[num].char_spd.z = - 2.5 ;
			
		}

		// 攻撃
		if( g_padstate[num].input & PAD_INPUT_B)
		{
			m_Player[num].animflg = pAttack ;
		}
	}
}

/* ===============================================================================
|
|   関数名	PlayerFallDown
|       処理内容  
|				  プレイヤーの判定と落下を行う
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void PlayerFallDown()
{
	// 画面外に落ちた場合
	for( int i = 0 ; i < MAX_PLAYER ; i++ )
	{
		if( m_Player[i].char_pos.x > STAGE_LIMIT_RIGHT )
		{
			m_Player[i].char_spd.x = 0.0f ;
			m_Player[i].char_spd.y = -7.0f ;
			m_Player[i].char_spd.z = 0.0f ;
			m_Player[i].char_rot += 0.1f ;
			m_Player[i].char_pos.x += 5.0f;
			m_Player[i].animflg = pDown ;
			m_Player[i].invi_flg = 1 ;
			m_Player[i].invi_count = 0 ;
			MV1SetVisible( m_Player[i].player_model , TRUE ) ;
			//if ( i == 0 )
			//	StartJoypadVibration(DX_INPUT_PAD1, 1000, 100, -1) ;
			//if ( i == 1 )
			//	StartJoypadVibration(DX_INPUT_PAD2, 1000, 100, -1) ;
			m_Player[i].death_count++ ;
			if ( i == PLAYER1 ){
				m_Player[PLAYER1].lastfall_flg = TRUE ;
				m_Player[PLAYER2].lastfall_flg = FALSE ;
			} else if ( i == PLAYER2 ){
				m_Player[PLAYER1].lastfall_flg = FALSE ;
				m_Player[PLAYER2].lastfall_flg = TRUE ;
			}
		}
		if( m_Player[i].char_pos.x < STAGE_LIMIT_LEFT)
		{
			m_Player[i].char_spd.x = 0.0f ;
			m_Player[i].char_spd.y = -7.0f ;
			m_Player[i].char_spd.z = 0.0f ;
			m_Player[i].char_rot += 0.1f ;
			m_Player[i].char_pos.x -= 5.0f;
			m_Player[i].animflg = pDown ;
			m_Player[i].invi_flg = 1 ;
			m_Player[i].invi_count = 0 ;
			MV1SetVisible( m_Player[i].player_model , TRUE ) ;
			//if ( i == 0 )
			//	StartJoypadVibration(DX_INPUT_PAD1, 100, 500) ;
			//if ( i == 1 )
			//	StartJoypadVibration(DX_INPUT_PAD2, 100, 500) ;
			m_Player[i].death_count++ ;
			if ( i == PLAYER1 ){
				m_Player[PLAYER1].lastfall_flg = TRUE ;
				m_Player[PLAYER2].lastfall_flg = FALSE ;
			} else if ( i == PLAYER2 ){
				m_Player[PLAYER1].lastfall_flg = FALSE ;
				m_Player[PLAYER2].lastfall_flg = TRUE ;
			}
		}
		if( m_Player[i].char_pos.z > STAGE_LIMIT_UP)
		{
			m_Player[i].char_spd.x = 0.0f ;
			m_Player[i].char_spd.y = -7.0f ;
			m_Player[i].char_spd.z = 0.0f ;
			m_Player[i].char_rot += 0.1f ;
			m_Player[i].char_pos.z += 5.0f;
			m_Player[i].animflg = pDown ;
			m_Player[i].invi_flg = 1 ;
			m_Player[i].invi_count = 0 ;
			MV1SetVisible( m_Player[i].player_model , TRUE ) ;
			//if ( i == 0 )
			//	StartJoypadVibration(DX_INPUT_PAD1, 100, 500) ;
			//if ( i == 1 )
			//	StartJoypadVibration(DX_INPUT_PAD2, 100, 500) ;
			m_Player[i].death_count++ ;
			if ( i == PLAYER1 ){
				m_Player[PLAYER1].lastfall_flg = TRUE ;
				m_Player[PLAYER2].lastfall_flg = FALSE ;
			} else if ( i == PLAYER2 ){
				m_Player[PLAYER1].lastfall_flg = FALSE ;
				m_Player[PLAYER2].lastfall_flg = TRUE ;
			}
		}
		if( m_Player[i].char_pos.z < STAGE_LIMIT_DOWN)
		{
			m_Player[i].char_spd.x = 0.0f ;
			m_Player[i].char_spd.y = -7.0f ;
			m_Player[i].char_spd.z = 0.0f ;
			m_Player[i].char_rot += 0.1f ;
			m_Player[i].char_pos.z -= 5.0f;
			m_Player[i].animflg = pDown ;
			m_Player[i].invi_flg = 1 ;
			m_Player[i].invi_count = 0 ;
			MV1SetVisible( m_Player[i].player_model , TRUE ) ;
			//if ( i == 0 )
			//	StartJoypadVibration(DX_INPUT_PAD1, 100, 500) ;
			//if ( i == 1 )
			//	StartJoypadVibration(DX_INPUT_PAD2, 100, 500) ;
			m_Player[i].death_count++ ;
			if ( i == PLAYER1 ){
				m_Player[PLAYER1].lastfall_flg = TRUE ;
				m_Player[PLAYER2].lastfall_flg = FALSE ;
			} else if ( i == PLAYER2 ){
				m_Player[PLAYER1].lastfall_flg = FALSE ;
				m_Player[PLAYER2].lastfall_flg = TRUE ;
			}
		}

		// --- 無敵発生
		if(m_Player[i].invi_flg == 2)
		{
			m_Player[i].invi_count++ ;
			if(m_Player[i].invi_count % 8 < 6 )
			{
				MV1SetVisible( m_Player[i].player_model , TRUE ) ;
			}
			else
			{
				MV1SetVisible( m_Player[i].player_model , FALSE ) ;
			}
//			printf( "P1 count = %d\n" , m_Player[PLAYER1].invi_count) ;
			if(m_Player[i].invi_count > 90)
			{
				 m_Player[i].invi_flg = 0 ;
			}

		}
	}
	
	
}