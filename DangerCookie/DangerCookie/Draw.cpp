/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

	FILE	: Draw.cpp

+ ------ Explanation of file --------------------------------------------------------------------------
       
	モデルの描画

+ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */

#include "Common.h"
#include "time.h"
// --- 初期座標のセット
VECTOR  pos[MAX_OBJ] ;

// --- モデルハンドル格納変数
int		stage_model ;
int		bg_model  ;
int		maguma_model ;
int     castleWall_model ;
int     sideCake_model ;

// --- SPEED UP 移動量と初期座標
int     grap_spd   = 20 ;
int     grap_pos_x = 2000 ;
int     grap_pos_y = 130 ;
int     grap_cnt   = 0 ;

// --- スコア 初期座標
int		score_pos_y = -280 ;
int     hour = 6 ;

// --- マグマのテクスチャ
int		GrHandle ;
int		TexIndex ;

// --- スコアUI関連
int		purasu[2] = {0,0} ;
int		change[3][10] ;

int     title_letter ;	 // --- タイトルの文字
int     title_main ;	 // --- メインタイトル

// --- タイトル 応援団
int     Title_Cheer[3]  ;
VECTOR  Title_Cheer_pos[3] ;
int     Right_Title_Cheer[3] ;
VECTOR  Right_Title_Cheer_pos[3] ;


// --- カウントダウンのUI
int		C_start[3] ;
double	C_start_scale[3] ;
double	C_start_rot[3]	;

int		C_startGo ;
double	C_startGo_scale ;

// --- フィニッシュ ハンドル 移動量 初期座標
int     finish_letter ;  
int     finish_ui[6]  ;
int     finish_pos_x[2] = { -2800 , 2500 } ;
int     finish_spd = 50 ;

// --- キャラ選択画面 READY、矢印、背景の画像
int     ready_letter  ;
int     left_arrow[2]  ;
int     right_arrow[2] ;
int     left_white_arrow[2] ;
int     right_white_arrow[2] ;
int     Select_face[3] ;
int     Select_cursor[2]  ;

int     cSelect_BG ;
int     cSelect_Face_BG[2] ;

// --- キャラ選択画面 AreYouREADY
int     AYR_ui[2] ;
int     AYR_pos[2] ;
int     AYR_spd  ;

// --- エフェクト
int     Atk_EffHandle ;
int     Atk_EffModel ;
VECTOR  Atk_Scale   ;

int		testflg = 0 ;
int		rootflm ;		 // --- ルートフレーム 
int		anim_flow ;		 // --- アニメーション
int		tmp_attachidx  ; // --- アニメーションの総時間獲得
float   anim_playtime  ; // --- アニメーション再生時間
float   anim_totaltime ; // --- アニメーション総時間
float   anim_spd ;       // --- アニメーションスピード

// --- キャラセレクト関連変数 --- //

// モデルの座標
VECTOR P1_pos , P2_pos ;
// 移動量
float  p1_Move_spd , p2_Move_spd ;

// モデルの回転
float  P_rot[2] ;
// 回転量
float  P1_Rot_spd  , P2_Rot_spd ;

int ShadowMapHandle ;

bool Spinflg = false ;

// resultで使用
float	End_Stage_Scale[2] = {1.0f , 1.0f};
float	sp_scale[2] = {0.3f,0.3f};

int		Win_model ;
int		Lose_model ;
int		End_BG ;
int		End_Effect[3] ;
int		End_Buck_Effect[5] ;
int		End_Stage[2] ;
VECTOR	es_pos[2] ;
float	char_pos_spd[2] = {19.0f,19.0f};
float	char_pos_y[2] = {0.0f,0.0f};
int		poscnt = 0 ;

int result_anim_cnt = 0;

int		cScore[2][3][10] ;				// --- 1P側スコア数字

int maku_spd_x = 30 ;

int Result_BG ;

int shadow_p ;							// --- プレイヤー影用

int effect_pos_y[2] = {1000 , -2000} ;
int effect_pos_x[2] = { -1200 , 1920 } ;
int effect_spd_y = 30 ;
int effect_spd_x = 20 ;

int change_angle = 0 ;

int M_Game_Start_SE ;		// --- GO!の効果音

/* ===============================================================================
|
|   関数名	Draw_Init_Model
|       処理内容  モデルの初期座標や大きさの指定をする関数
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Draw_Init_Model()
{

	M_Game_Start_SE		= LoadSoundMem( "..\\Data\\Sound\\M_Game\\game_start_se.mp3" ) ;	// --- GO!の効果音

	// --- エフェクト関連 -------------------------------

	Atk_EffHandle = MV1LoadModel("..\\Data\\UI\\Efect.mqo")  ;
	Atk_EffModel  =  Atk_EffHandle ;
	Atk_Scale = VGet( 1.1f , 1.1f , 1.1f) ;
	MV1SetScale( Atk_EffModel , Atk_Scale ) ;

	// --- UI関連 -------------------------------

	score[PLAYER1][0] = LoadGraph("..\\Data\\UI\\score11.png") ;
	score[PLAYER1][3] = LoadGraph("..\\Data\\UI\\score11_last.png") ;
	score[PLAYER1][1] = LoadGraph("..\\Data\\UI\\score12.png") ;
	score[PLAYER1][4] = LoadGraph("..\\Data\\UI\\score12_last.png") ;
	score[PLAYER1][2] = LoadGraph("..\\Data\\UI\\score13.png") ;
	score[PLAYER1][5] = LoadGraph("..\\Data\\UI\\score13_last.png") ;
	score[PLAYER2][0] = LoadGraph("..\\Data\\UI\\score21.png") ;
	score[PLAYER2][3] = LoadGraph("..\\Data\\UI\\score21_last.png") ;
	score[PLAYER2][1] = LoadGraph("..\\Data\\UI\\score22.png") ;
	score[PLAYER2][4] = LoadGraph("..\\Data\\UI\\score22_last.png") ;
	score[PLAYER2][2] = LoadGraph("..\\Data\\UI\\score23.png") ;
	score[PLAYER2][5] = LoadGraph("..\\Data\\UI\\score23_last.png") ;

	LoadDivGraph( "..\\Data\\UI\\number_1.png" , 10 , 10 , 1 , 88 , 170 ,cScore[PLAYER1][PLAYER1]) ;
	LoadDivGraph( "..\\Data\\UI\\number_2.png" , 10 , 10 , 1 , 88 , 170 ,cScore[PLAYER1][PLAYER2]) ;
	LoadDivGraph( "..\\Data\\UI\\number_3.png" , 10 , 10 , 1 , 88 , 170 ,cScore[PLAYER1][PLAYER3]) ;
	LoadDivGraph( "..\\Data\\UI\\number_1.png" , 10 , 10 , 1 , 88 , 170 ,cScore[PLAYER2][PLAYER1]) ;
	LoadDivGraph( "..\\Data\\UI\\number_2.png" , 10 , 10 , 1 , 88 , 170 ,cScore[PLAYER2][PLAYER2]) ;
	LoadDivGraph( "..\\Data\\UI\\number_3.png" , 10 , 10 , 1 , 88 , 170 ,cScore[PLAYER2][PLAYER3]) ;


	LoadDivGraph( "..\\Data\\UI\\clock_number_1.png" , 10 , 10 , 1 , 88 , 170 ,mc) ;
	LoadDivGraph( "..\\Data\\UI\\clock_number_1.png" , 10 , 10 , 1 , 88 , 170 ,me) ;
	LoadDivGraph( "..\\Data\\UI\\clock_number_2.png" , 10 , 10 , 1 , 88 , 170 ,md) ;
	LoadDivGraph( "..\\Data\\UI\\clock_number_3.png" , 10 , 10 , 1 , 88 , 170 ,my) ;
	LoadDivGraph( "..\\Data\\UI\\clock_number_4.png" , 10 , 10 , 1 , 88 , 170 ,mr) ;


	TimeHandle = LoadGraph("..\\Data\\UI\\clock (2) (1).png") ;
	Speed_Grp = LoadGraph("..\\Data\\UI\\speedTag.png") ;

	LoadDivGraph("..\\Data\\UI\\Finish.png" , 6 , 1 , 6 , 2300 ,  55 , finish_ui) ;

	// --- カウントダウン
	LoadDivGraph( "..\\Data\\UI\\CountDown.png" , 3 , 3 , 1 , 218 , 260 , C_start) ;
	C_start_scale[0] = 0.2f ;
	C_start_rot[0] = 0.0f ;

	C_start_scale[1] = 0.2f ;
	C_start_rot[1] = 0.0f ;

	C_start_scale[2] = 0.5f ;
	C_start_rot[2] = 0.0f ;

	C_startGo        = LoadGraph("..\\Data\\UI\\go.png") ;
	C_startGo_scale  = 0.2f ;

	// --- ステージ関連 -------------------------

	pos[0] = VGet( 400.0f , 250.0f  , -150.0f ) ;		// ステージの座標 
	pos[1] = VGet( 400.0f , -50.0f , 4000.0f ) ;		// 背景の座標
	pos[2] = VGet( 800.0f   , -1950.0f  , -500.0f ) ;  // マグマの座標
				
	pos[3] = VGet( -2000.0f , 80.0f , 2600.0f ) ;     // 左の城壁の座標
	pos[4] = VGet(  2700.0f , 80.0f , 2600.0f ) ;     // 右の      〃

	pos[5] = VGet( -1250.0f  , 150.0f , 1600.0f ) ;     // 左のケーキの座標
	pos[6] = VGet(  2070.0f , 150.0f , 1600.0f ) ;     // 右の      〃

	stage_model = MV1LoadModel("..\\Data\\Stage\\floor.mqo") ;
	Model[0] = stage_model ;

	bg_model = MV1LoadModel("..\\Data\\BackGround\\SweetCastle.mv1") ;
	Model[1] = bg_model ;

	castleWall_model = MV1LoadModel("..\\Data\\Stage\\CastleWall.mqo") ;
	Model[3] = castleWall_model ; 
	Model[4] = MV1DuplicateModel( Model[3] ) ;

	sideCake_model = MV1LoadModel("..\\Data\\Stage\\SideCake.mqo") ;
	Model[5] = sideCake_model ; 
	Model[6] = MV1DuplicateModel( Model[5] ) ; 

	// --- 大きさ設定
	MV1SetScale( Model[0] , VGet( 4.1f , 1.0f , 4.0f) ) ;  // ステージの大きさ
	MV1SetScale( Model[1] , VGet( 1.5f , 0.9f, 0.8f) )  ;  // 背景の大きさ;  

	MV1SetScale( Model[3] , VGet( 0.6f , 0.7f , 1.5f )) ;  // 左の城壁の大きさ
	MV1SetScale( Model[4] , VGet( 0.6f , 0.7f , 1.5f )) ;  // 右の      〃

	MV1SetScale( Model[5] , VGet( 7.6f , 5.5f , 6.5f )) ;  // 左のケーキの大きさ
	MV1SetScale( Model[6] , VGet( 7.7f , 5.5f , 6.5f )) ;  // 右の      〃

    // --- マグマ関連 -------------------------------------------------

	maguma_model = MV1LoadModel( "..\\Data\\Stage\\choco.mv1" ) ;
	Model[2] = maguma_model ;
	// 画像の読みこみ
	GrHandle = LoadGraph( "..\\Data\\Stage\\chocolate.jpg" ) ;
	 // テクスチャの番号を取得 番号はModelViwerで確認
	TexIndex = MV1GetMaterialDifMapTexture( maguma_model, 0 ) ;
	// テクスチャで使用するグラフィックハンドルを変更する
    MV1SetTextureGraphHandle( maguma_model, TexIndex, GrHandle, FALSE ) ;

	// アニメーション関連
	anim_flow      = MV1LoadModel( "..\\Data\\Stage\\choco_anim.mv1" ) ;
	tmp_attachidx  = MV1AttachAnim( maguma_model , 0 , anim_flow ) ; 
	anim_totaltime = MV1GetAttachAnimTotalTime( maguma_model , tmp_attachidx) ;
	anim_spd = 0.008f ;
	anim_playtime = 15.0f ;

	MV1SetScale( Model[2] , VGet( 6.5f , 0.9f , 2.5f) ) ;  // マグマ

	// --- キャラセレクト -----------------------

	for( int i = 0 ; i < MAX_PLAYER ; i++ )
	{
		m_Player[i].pSelect_Model[0] = MV1LoadModel( PLAYER_SELECT_1_S ) ; 
		m_Player[i].sp_Model[0] = m_Player[i].pSelect_Model[0] ;								
		m_Player[i].pSelect_Model[1] = MV1LoadModel( PLAYER_SELECT_2_S ) ; 
		m_Player[i].sp_Model[1] = m_Player[i].pSelect_Model[1] ;								
		m_Player[i].pSelect_Model[2] = MV1LoadModel( PLAYER_SELECT_3_S ) ;
		m_Player[i].sp_Model[2] = m_Player[i].pSelect_Model[2] ;							
		m_Player[i].p_flg = 0 ;
		m_Player[i].sp_flg = m_Player[i].p_flg ;
	}

	// --------------------------------------- 応援団(旗) -----------------------------------------------//

	// --- 応援団の位置と回転
	// --- 応援団の位置と回転
	f_Player[PLAYER1][0].char_pos =  VGet( -1000.0f , 250.0f , 0.0f ) ;			// プレイヤー1の初期座標
	f_Player[PLAYER1][1].char_pos =  VGet( -1000.0f , 250.0f , 750.0f ) ;		// プレイヤー1の初期座標
	f_Player[PLAYER1][2].char_pos =  VGet( -1000.0f , 250.0f , 1500.0f ) ;		// プレイヤー1の初期座標
	f_Player[PLAYER1][3].char_pos =  VGet( -1000.0f , 250.0f , 2250.0f ) ;		// プレイヤー1の初期座標

	f_Player[PLAYER2][0].char_pos =  VGet( 1800.0f , 250.0f , 0.0f ) ;			// プレイヤー2の初期座標
	f_Player[PLAYER2][1].char_pos =  VGet( 1800.0f , 250.0f , 750.0f ) ;		// プレイヤー2の初期座標
	f_Player[PLAYER2][2].char_pos =  VGet( 1800.0f , 250.0f , 1500.0f ) ;		// プレイヤー2の初期座標
	f_Player[PLAYER2][3].char_pos =  VGet( 1800.0f , 250.0f , 2250.0f ) ;		// プレイヤー2の初期座標
	f_Player[PLAYER1][0].char_rot = -45.0f ;									// プレイヤー1の角度
	f_Player[PLAYER1][1].char_rot = -45.0f ;									// プレイヤー1の角度
	f_Player[PLAYER1][2].char_rot = -45.0f ;									// プレイヤー1の角度
	f_Player[PLAYER1][3].char_rot = -45.0f ;									// プレイヤー1の角度
	f_Player[PLAYER2][0].char_rot = 45.0f ;										// プレイヤー2の角度
	f_Player[PLAYER2][1].char_rot = 45.0f ;										// プレイヤー2の角度
	f_Player[PLAYER2][2].char_rot = 45.0f ;										// プレイヤー2の角度
	f_Player[PLAYER2][3].char_rot = 45.0f ;										// プレイヤー2の角度
	f_Player[PLAYER1][0].anim_spd = 0.9f ;									// プレイヤー1の角度
	f_Player[PLAYER1][1].anim_spd = 0.9f ;									// プレイヤー1の角度
	f_Player[PLAYER1][2].anim_spd = 0.9f ;									// プレイヤー1の角度
	f_Player[PLAYER1][3].anim_spd = 0.9f ;									// プレイヤー1の角度
	f_Player[PLAYER2][0].anim_spd = 0.9f ;										// プレイヤー2の角度
	f_Player[PLAYER2][1].anim_spd = 0.9f ;										// プレイヤー2の角度
	f_Player[PLAYER2][2].anim_spd = 0.9f ;										// プレイヤー2の角度
	f_Player[PLAYER2][3].anim_spd = 0.9f ;										// プレイヤー2の角度

	// --- 応援団のアニメーション再生速度

	// --- 応援団のアニメーションフラグ
	f_Player[PLAYER1][0].animflg = pStand ;
	f_Player[PLAYER2][0].animflg = pStand ;

	// ------------------------------------------------------------------------------------------------------------------

	// --------------------------------------- 応援団(団員) -----------------------------------------------//

	// --- 応援団の位置と回転
	o_Player[PLAYER1].char_pos =  VGet( -700.0f , 250.0f , 1200.0f ) ;		// プレイヤー1の初期座標
	o_Player[PLAYER2].char_pos =  VGet( 1500.0f , 250.0f , 1200.0f ) ;		// プレイヤー1の初期座標
	o_Player[PLAYER1].char_rot = -45.0f ;									// プレイヤー1の角度
	o_Player[PLAYER2].char_rot = 45.0f ;									// プレイヤー1の角度

	// --- 応援団のアニメーション再生速度
	o_Player[PLAYER1].anim_spd = 0.8f ;
	o_Player[PLAYER2].anim_spd = 0.8f ;

	// --- 応援団のアニメーションフラグ
	o_Player[PLAYER1].animflg = pStand ;
	o_Player[PLAYER2].animflg = pStand ;

	// ------------------------------------------------------------------------------------------------------------------

	// --- 角度
	P_rot[PLAYER1] = -0.3f ;
	P_rot[PLAYER2] = 0.3f ;
	P1_Rot_spd       = -0.05f ;
	P2_Rot_spd       =  0.05f ;

	// --- 上下移動
	m_Player[PLAYER1].select_C_pos = VGet( 140.0f , 260.0f , -480.0f ); 
	m_Player[PLAYER2].select_C_pos = VGet( 680.0f , 260.0f , -480.0f );

	p1_Move_spd  =   6.0f ;
	p2_Move_spd  =   6.0f ;

	// --- 壁関連 -------------------------------
	collapse_Wall[0].wall_model = MV1LoadModel( "..\\Data\\Wall\\Wall_0.mv1" ) ;
	collapse_Wall[0].w_Model = collapse_Wall[0].wall_model ;

	collapse_Wall[1].wall_model = MV1LoadModel( "..\\Data\\Wall\\Wall_1.mv1" ) ;
	collapse_Wall[1].w_Model = collapse_Wall[1].wall_model ;

	collapse_Wall[2].wall_model = MV1LoadModel( "..\\Data\\Wall\\Wall_2.mv1" ) ;
	collapse_Wall[2].w_Model = collapse_Wall[2].wall_model ;

	collapse_Wall[3].wall_model = MV1LoadModel( "..\\Data\\Wall\\Wall_3.mv1" ) ;
	collapse_Wall[3].w_Model = collapse_Wall[3].wall_model ;

	collapse_Wall[4].wall_model = MV1LoadModel( "..\\Data\\Wall\\Wall_4.mv1" ) ;
	collapse_Wall[4].w_Model = collapse_Wall[4].wall_model ;

	// --- コリジョンのための壁
	collision_Wall[0].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_0.mv1" ) ;
	collision_Wall[0].w_Model = collision_Wall[0].wall_model ;

	collision_Wall[1].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_1.mv1" ) ;
	collision_Wall[1].w_Model = collision_Wall[1].wall_model ;

	collision_Wall[2].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_2.mv1" ) ;
	collision_Wall[2].w_Model = collision_Wall[2].wall_model ;

	collision_Wall[3].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_3.mv1" ) ;
	collision_Wall[3].w_Model = collision_Wall[3].wall_model ;

	collision_Wall[4].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_4.mv1" ) ;
	collision_Wall[4].w_Model = collision_Wall[4].wall_model ;
	MV1SetupCollInfo( collision_Wall[0].wall_model, -1 ) ;
	MV1SetupCollInfo( collision_Wall[1].wall_model, -1 ) ;
	MV1SetupCollInfo( collision_Wall[2].wall_model, -1 ) ;
	MV1SetupCollInfo( collision_Wall[3].wall_model, -1 ) ;
	MV1SetupCollInfo( collision_Wall[4].wall_model, -1 ) ;

	// --- 壁(影)関連 -------------------------------
	shadow_Wall[0].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_0.mv1" ) ;
	shadow_Wall[0].w_Model = shadow_Wall[0].wall_model ;

	shadow_Wall[1].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_1.mv1" ) ;
	shadow_Wall[1].w_Model = shadow_Wall[1].wall_model ;

	shadow_Wall[2].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_2.mv1" ) ;
	shadow_Wall[2].w_Model = shadow_Wall[2].wall_model ;

	shadow_Wall[3].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_3.mv1" ) ;
	shadow_Wall[3].w_Model = shadow_Wall[3].wall_model ;

	shadow_Wall[4].wall_model = MV1LoadModel( "..\\Data\\Wall\\CWall_4.mv1" ) ;
	shadow_Wall[4].w_Model = shadow_Wall[4].wall_model ;
	
	// --- プレイヤー(影) -----------------------------------------------------
	shadow_p = MV1LoadModel( "..\\Data\\Player\\shadow_p.mv1" ) ;

	// ------------------------------------------------------------------------
	for ( int i = 0 ; i < MAX_WALL ; i++ )
		collision_Wall[i].w_pos =  VGet( 0.0f, 0.0f, 2000.0f) ;		// 壁の初期座標

	// 壁の最大表示数
	wposNo[0] = rand() % 3 ;		// --- 壁の位置
	wposNo[1] = rand() % 3 ;		// --- 壁の位置	
	wposNo[2] = rand() % 3 ;		// --- 壁の位置	

	while ( wposNo[1] == wposNo[0] ){	// --- 被らないような壁の位置
		wposNo[1] = rand() % 3 ;	// --- 壁の位置
	}

	while ( wposNo[2] == wposNo[0] || wposNo[2] == wposNo[1] ){
		wposNo[2] = rand() % 3 ;	// --- 壁の位置
	}

	cnt = rand() % 3 ;
	while ( cnt == 0 ){
		cnt = rand() % 3 ;		// --- 壁が１枚以上とする処理
	}

	for ( int i = 0 ; i < cnt ; i++ ){
		element[i] = rand() % MAX_WALL ;
		if ( i == 1 && cnt >= 2 ){
			while (element[i-1] == element[i]){
				element[i] = rand() % MAX_WALL ;
			}
		}
		if ( i == 2 && cnt == 3 ){
			while (element[i-1] == element[i] || element[i-2] == element[i] ){
				element[i] = rand() % MAX_WALL ;
			}
		}
		collapse_Wall[element[i]].Init( wposNo[i] )  ;
		collision_Wall[element[i]].Init( wposNo[i] )  ;
		shadow_Wall[element[i]].Init( wposNo[i] )  ;
	}

	for(int i = 0 ; i < 10 ; i++ ){
		change[0][i] = md[i] ;
	}
	for(int i = 0 ; i < 10 ; i++ ){
		md[(i+1)%10] = change[0][i] ;
	}

	for(int i = 0 ; i < 10 ; i++ ){
		change[1][i] = my[i] ;
	}
	for(int i = 0 ; i < 10 ; i++ ){
		my[(i+1)%10] = change[1][i] ;
	}

	for(int i = 0 ; i < 10 ; i++ ){
		change[2][i] = mr[i] ;
	}
	for(int i = 0 ; i < 10 ; i++ ){
		mr[(i+1)%10] = change[2][i] ;
	}

}

/* ===============================================================================
|
|   関数名	CheeringInit
|       処理内容  応援団の読み込み
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void CheeringInit()
{
	srand( (unsigned)time(NULL) ) ;

	// --- 応援団の読み込み
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		switch( m_Player[i].p_flg ){
			case 0 :
				// --------------------------------　旗持ち　----------------------------------------------------------------//
				for ( int y = 0 ; y < 4 ; y++ ){
					f_Player[i][y].player_model =  MV1LoadModel("..\\Data\\Player\\M_1P\\mv1\\1P_Flag_Model.mv1") ;	
					f_Player[i][y].rootflm = MV1SearchFrame(f_Player[i][y].player_model,"obj_2") ;								// --- ボーンの番号を返す
					f_Player[i][y].p_Model = f_Player[i][y].player_model ;														// --- モデル情報格納

					// --- 初期アニメーションセット(nutralに入れている)
					f_Player[i][y].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\1P_Flag_Anim.mv1" ) ;				// --- 静止アニメーション
					f_Player[i][y].tmp_attachidx = MV1AttachAnim(f_Player[i][y].player_model,0,f_Player[i][y].anim_nutral) ;					// --- モデルにアタッチ
					f_Player[i][y].anim_totaltime = MV1GetAttachAnimTotalTime(f_Player[i][y].player_model,f_Player[i][y].tmp_attachidx) ;		// --- アニメーション総時間取得

					if ( y % 2 == 0 )
						f_Player[i][y].anim_playtime = f_Player[i][y].anim_totaltime / 2 ;
				}

				// --------------------------------　応援団　----------------------------------------------------------------//
				o_Player[i].player_model =  MV1LoadModel("..\\Data\\Player\\M_1P\\mv1\\Tasuki_1P.mv1") ;	
				o_Player[i].rootflm = MV1SearchFrame(o_Player[i].player_model,"obj_2") ;											// --- ボーンの番号を返す
				o_Player[i].p_Model = o_Player[i].player_model ;																	// --- モデル情報格納

				// --- 初期アニメーションセット(nutralに入れている)
				o_Player[i].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\Tasuki_1P_Anim.mv1" ) ;						// --- 静止アニメーション
				o_Player[i].tmp_attachidx = MV1AttachAnim(o_Player[i].player_model,0,o_Player[i].anim_nutral) ;						// --- モデルにアタッチ
				o_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(o_Player[i].player_model,o_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				break ;

			case 1 :
				// --------------------------------　旗持ち　----------------------------------------------------------------//
				for ( int y = 0 ; y < 4 ; y++ ){
					f_Player[i][y].player_model =  MV1LoadModel("..\\Data\\Player\\M_2P\\mv1\\2P_Flag_Model.mv1") ;	
					f_Player[i][y].rootflm = MV1SearchFrame(f_Player[i][y].player_model,"obj_2") ;								// --- ボーンの番号を返す
					f_Player[i][y].p_Model = f_Player[i][y].player_model ;														// --- モデル情報格納

					// --- 初期アニメーションセット(nutralに入れている)
					f_Player[i][y].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\2P_Flag_Anim.mv1" ) ;								// --- 静止アニメーション
					f_Player[i][y].tmp_attachidx = MV1AttachAnim(f_Player[i][y].player_model,0,f_Player[i][y].anim_nutral) ;					// --- モデルにアタッチ
					f_Player[i][y].anim_totaltime = MV1GetAttachAnimTotalTime(f_Player[i][y].player_model,f_Player[i][y].tmp_attachidx) ;		// --- アニメーション総時間取得

					if ( y % 2 == 0 )
						f_Player[i][y].anim_playtime = f_Player[i][y].anim_totaltime / 2 ;
				}

				// --------------------------------　応援団　----------------------------------------------------------------//
				o_Player[i].player_model =  MV1LoadModel("..\\Data\\Player\\M_2P\\mv1\\Tasuki_2P.mv1") ;	
				o_Player[i].rootflm = MV1SearchFrame(o_Player[i].player_model,"obj_2") ;												// --- ボーンの番号を返す
				o_Player[i].p_Model = o_Player[i].player_model ;																		// --- モデル情報格納

				// --- 初期アニメーションセット(nutralに入れている)
				o_Player[i].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\Tasuki_2P_Anim.mv1" ) ;							// --- 静止アニメーション
				o_Player[i].tmp_attachidx = MV1AttachAnim(o_Player[i].player_model,0,o_Player[i].anim_nutral) ;							// --- モデルにアタッチ
				o_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(o_Player[i].player_model,o_Player[i].tmp_attachidx) ;			// --- アニメーション総時間取得
				break ;

			case 2 :
				// --------------------------------　旗持ち　----------------------------------------------------------------//
				for ( int y = 0 ; y < 4 ; y++ ){
					f_Player[i][y].player_model =  MV1LoadModel("..\\Data\\Player\\M_3P\\mv1\\3P_Flag_Model.mv1") ;	
					f_Player[i][y].rootflm = MV1SearchFrame(f_Player[i][y].player_model,"obj_2") ;											// --- ボーンの番号を返す
					f_Player[i][y].p_Model = f_Player[i][y].player_model ;																	// --- モデル情報格納

					// --- 初期アニメーションセット(nutralに入れている)
					f_Player[i][y].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\3P_Flag_Anim.mv1" ) ;							// --- 静止アニメーション
					f_Player[i][y].tmp_attachidx = MV1AttachAnim(f_Player[i][y].player_model,0,f_Player[i][y].anim_nutral) ;				// --- モデルにアタッチ
					f_Player[i][y].anim_totaltime = MV1GetAttachAnimTotalTime(f_Player[i][y].player_model,f_Player[i][y].tmp_attachidx) ;	// --- アニメーション総時間取得

					if ( y % 2 == 0 )
						f_Player[i][y].anim_playtime = f_Player[i][y].anim_totaltime / 2 ;
				}

				// --------------------------------　応援団　----------------------------------------------------------------//
				o_Player[i].player_model =  MV1LoadModel("..\\Data\\Player\\M_3P\\mv1\\Tasuki_3P.mv1") ;	
				o_Player[i].rootflm = MV1SearchFrame(o_Player[i].player_model,"obj_2") ;											// --- ボーンの番号を返す
				o_Player[i].p_Model = o_Player[i].player_model ;																	// --- モデル情報格納

				// --- 初期アニメーションセット(nutralに入れている)
				o_Player[i].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\Tasuki_3P_Anim.mv1" ) ;						// --- 静止アニメーション
				o_Player[i].tmp_attachidx = MV1AttachAnim(o_Player[i].player_model,0,o_Player[i].anim_nutral) ;						// --- モデルにアタッチ
				o_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(o_Player[i].player_model,o_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				break ;
		}
	}


}

/* ===============================================================================
|
|   関数名	DrawGameModel
|       処理内容  モデルを描画する関数 大きさの変更
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void DrawGameModel()
{
	DrawBox(0,0,1920,1080,GetColor(200,200,255),true) ; //最後の引数をfalseにすると塗りつぶし無し

	// --- 背景などのオブジェクトの描画
	
	MV1SetAttachAnimTime( maguma_model , tmp_attachidx , anim_playtime) ;

	for( int i = 0 ; i < MAX_OBJ ; i++ )
	{
		MV1SetPosition(Model[i] , pos[i] ) ;
		MV1DrawModel(Model[i]) ;
	}

	MV1SetRotationXYZ( Model[5] , VGet(0.0f, 1.57f * 0.2f, 0.0f) ) ;
	MV1SetRotationXYZ( Model[6] , VGet(0.0f, 1.57f * 1.8f, 0.0f) ) ;

	// --- 応援団(旗)の描画
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		for( int y = 0 ; y < 4 ; y++ ){
			MV1SetScale( f_Player[i][y].p_Model , VGet( 1.0f , 1.0f , 1.0f) ) ;
			MV1SetAttachAnimTime(f_Player[i][y].p_Model, f_Player[i][y].tmp_attachidx  ,f_Player[i][y].anim_playtime) ;		// アニメーションの何番目（何秒後）の動きか同期させる
			MV1SetRotationXYZ( f_Player[i][y].p_Model, VGet(0.0f, f_Player[i][y].char_rot , 0.0f ) ) ;
			MV1SetPosition(f_Player[i][y].p_Model , VGet(f_Player[i][y].char_pos.x ,f_Player[i][y].char_pos.y ,f_Player[i][y].char_pos.z) ) ;
			MV1DrawModel(f_Player[i][y].p_Model) ;

			// 影　--------------------------------------------------------------------------------------------------------------------------------------------------------------
			MV1SetScale( shadow_p , VGet( 1.0f , 1.0f , 1.0f) ) ;
			MV1SetRotationXYZ( shadow_p, VGet(0.0f, f_Player[i][y].char_rot , 0.0f ) ) ;
			MV1SetPosition(shadow_p , VGet(f_Player[i][y].char_pos.x ,(f_Player[i][y].char_pos.y-50) ,f_Player[i][y].char_pos.z ) ) ;
			MV1DrawModel(shadow_p) ;
		}
	}

	// --- 応援団(団員)の描画
	for(int y = 0 ; y < 2 ; y++ ){
		for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
			MV1SetScale( o_Player[i].p_Model , VGet( 1.0f , 1.0f , 1.0f) ) ;
			MV1SetAttachAnimTime(o_Player[i].p_Model, o_Player[i].tmp_attachidx  ,o_Player[i].anim_playtime) ;		// アニメーションの何番目（何秒後）の動きか同期させる
			MV1SetRotationXYZ( o_Player[i].p_Model, VGet(0.0f, o_Player[i].char_rot , 0.0f ) ) ;
			MV1SetPosition(o_Player[i].p_Model , VGet(o_Player[i].char_pos.x ,o_Player[i].char_pos.y ,(o_Player[i].char_pos.z*(y+1)) ) ) ;
			MV1DrawModel(o_Player[i].p_Model) ;

			// 影--------------------------------------------------------------------------------------------------------------------------------------------------------------
			MV1SetScale( shadow_p , VGet( 1.0f , 1.0f , 1.0f) ) ;
			MV1SetRotationXYZ( shadow_p, VGet(0.0f, o_Player[i].char_rot , 0.0f ) ) ;
			MV1SetPosition(shadow_p , VGet(o_Player[i].char_pos.x ,(o_Player[i].char_pos.y-50) ,(o_Player[i].char_pos.z*(y+1)) ) ) ;
			MV1DrawModel(shadow_p) ;
		}
	}

	// --- プレイヤーの描画
	for( int i = 0 ; i < MAX_PLAYER ; i++ )
	{
		MV1SetScale( m_Player[i].p_Model , VGet( 0.6f , 0.6f , 0.6f) ) ;
		MV1SetAttachAnimTime(m_Player[i].p_Model, m_Player[i].tmp_attachidx  ,m_Player[i].anim_playtime) ;		// アニメーションの何番目（何秒後）の動きか同期させる
		m_Player[i].SetMovePosition() ;
		MV1SetRotationXYZ( m_Player[i].p_Model, VGet(0.0f, 1.57f * m_Player[i].char_rot , 0.0f ) ) ;
		MV1SetPosition(m_Player[i].p_Model , m_Player[i].char_pos ) ;
		MV1DrawModel(m_Player[i].p_Model) ;

		// --- プレイヤーの影
		if( m_Player[i].invi_flg != 1 ){																		// --- 落ちてない間描画
			MV1SetScale( shadow_p , VGet( 0.6f , 0.6f , 0.6f) ) ;
			MV1SetRotationXYZ( shadow_p, VGet(0.0f, 1.57f * m_Player[i].char_rot , 0.0f ) ) ;
			MV1SetPosition(shadow_p , VGet( m_Player[i].char_pos.x , 275.0f , (m_Player[i].char_pos.z-10)) ) ;
			MV1DrawModel(shadow_p) ;
		}
	}

	// --- 壁の描画処理
	for( int i = 0 ; i < cnt ; i++ )
	{
		MV1SetScale( collapse_Wall[element[i]].w_Model, VGet( 1.5f , 2.085f , 1.0f) ) ;
		MV1SetRotationXYZ( collapse_Wall[element[i]].w_Model, VGet(collapse_Wall[element[i]].w_rot, 0.0f, 0.0f) ) ;
		MV1SetPosition( collapse_Wall[element[i]].w_Model, collapse_Wall[element[i]].w_pos ) ;
		MV1DrawModel( collapse_Wall[element[i]].w_Model ) ;

		// --- コリジョンモデル
		MV1SetScale( collision_Wall[element[i]].w_Model, VGet( 1.5f , 2.085f , 1.0f) ) ;
		MV1SetRotationXYZ( collision_Wall[element[i]].w_Model, VGet(collision_Wall[element[i]].w_rot, 0.0f, 0.0f) ) ;
		MV1SetPosition( collision_Wall[element[i]].w_Model, collision_Wall[element[i]].w_pos ) ;
		MV1RefreshCollInfo( collision_Wall[element[i]].w_Model, -1 ) ;
//		MV1DrawModel( collision_Wall[element[i]].w_Model ) ;

		// --- 影の生成
		MV1SetScale( shadow_Wall[element[i]].w_Model, VGet( 1.5f , shadow_Wall[element[i]].scale_y , 1.0f) ) ;
		MV1SetRotationXYZ( shadow_Wall[element[i]].w_Model, VGet(86.4f, 0.0f, 0.0f) ) ;

		MV1SetPosition( shadow_Wall[element[i]].w_Model, shadow_Wall[element[i]].w_pos ) ;
		if( collapse_Wall[element[i]].w_rot > 86.4 ){
			MV1DrawModel( shadow_Wall[element[i]].w_Model ) ;
		}
		// ３Ｄモデルの不透明度を設定する
		MV1SetOpacityRate( shadow_Wall[element[i]].w_Model , Transparency ) ;

	}

	if(startFlg== true )
	{
		// --- UIの描画処理
		tennsuu() ;

		// --- スコア 外枠
		DrawGraph(0, 0, score[0][m_Player[PLAYER1].p_flg], true);
		DrawGraph(1690, 0, score[1][m_Player[PLAYER2].p_flg], true);

		// ---プレイヤー1の点数 
		DrawGraph( 50 , 50 , cScore[PLAYER1][m_Player[PLAYER1].p_flg][(p_score[0]+purasu[0])%10], true);
		DrawGraph(140 , 50 , cScore[PLAYER1][m_Player[PLAYER1].p_flg][p_score[0]%10], true);

		// --- プレイヤー2の点数 
		DrawGraph(1710 , 50 , cScore[PLAYER2][m_Player[PLAYER2].p_flg][(p_score[1]+purasu[1])%10], true);
		DrawGraph(1800 , 50 , cScore[PLAYER2][m_Player[PLAYER2].p_flg][p_score[1]%10], true);

		// --- ???表示
		DrawGraph(0, score_pos_y, score[0][m_Player[PLAYER1].p_flg + 3], true);			// --- 1Pのスコアの枠(最後)
		DrawGraph(1690, score_pos_y, score[1][m_Player[PLAYER2].p_flg + 3], true);		// --- 2Pのスコアの枠(最後)

		// --- 時計
		DrawGraph( 825 , 0 , TimeHandle , true) ;

		if(timer.tk_time > 40){
			DrawGraph( 865 , 0 , md[hour], true);									// --- Timer(十の位)
			DrawGraph( 965 , 0 , md[timer.tk_time%10], true);						// --- Timer(一の位)
		}

		// --- スピード
		DrawGraph( grap_pos_x , grap_pos_y , Speed_Grp , true ) ;

		// --- 10秒経過ごとの二桁目
		if(timer.tk_time < timer.c_time){
			hour-- ;
			timer.c_time -= 10 ;
		}
		if(hour < 2 && score_pos_y <= 0){
			score_pos_y++ ;
		}
		if ( timer.tk_time > 0 ){
			if(timer.tk_time <= 40 && timer.tk_time > 20){
				DrawGraph( 865 , 0 , my[hour], true);									// --- Timer(十の位)
				DrawGraph( 965 , 0 , my[timer.tk_time%10], true);						// --- Timer(一の位)
			}

			if(timer.tk_time <= 20){
				DrawGraph( 865 , 0 , mr[hour], true);									// --- Timer(十の位)
				DrawGraph( 965 , 0 , mr[timer.tk_time%10], true);						// --- Timer(一の位)
			}
		} else if ( timer.tk_time <= 0 ){
			DrawGraph( 865 , 0 , mr[0], true);									// --- Timer(十の位)
			DrawGraph( 965 , 0 , mr[0], true);						// --- Timer(一の位)
		}
	}

	// --- 20秒経過したら
	if( grap_cnt < 2)
	{
		if( timer.u_time >= 20 && grap_pos_x >= -4500)
		{
			grap_pos_x -= grap_spd ;
		}
		if( grap_pos_x <= -4000)
		{
			grap_cnt++ ;
			grap_pos_x = 2000 ;
			timer.u_time = 4 ;
		}
	}

	// --- エフェクトの描画
	DrawEffect() ;

	// --- フィニッシュ画像の描画
	DrawGraph( finish_pos_x[0] , 330  ,  finish_ui[0] , true );
	DrawGraph( finish_pos_x[1] , 385 ,   finish_ui[1] , true );
	DrawGraph( finish_pos_x[0] , 440  ,  finish_ui[2] , true );
	DrawGraph( finish_pos_x[1] , 495 ,   finish_ui[3] , true );
	DrawGraph( finish_pos_x[0] , 550  ,  finish_ui[4] , true );
	DrawGraph( finish_pos_x[1] , 605 ,   finish_ui[5] , true );

	if( timer.g_time >= 60 )
	{	
		finish_pos_x[0] += finish_spd ; 
		finish_pos_x[1] -= finish_spd ; 

		if( finish_pos_x[0] >= -150 && finish_pos_x[1] <= -150 )
		{
			finish_spd  = 0 ;
		}
	}

// --- カウントダウン
	if( timer.d_time >= 0 && timer.d_time <= 1 )
	{
		DrawRotaGraph( 1300 , 550 , C_start_scale[0] , C_start_rot[0] , C_start[0] , TRUE , FALSE ) ;
		C_start_scale[0] += 0.02 ;
		C_start_rot[0]   += 0.07  ;
		if( C_start_scale[0] >= 2.0f )
		{
			C_start_scale[0] = 2.0f ;
			C_start_rot[0] = 0.0f ;
		}
	}
	if( timer.d_time > 1 && timer.d_time <= 5 )
	{
		DrawRotaGraph( 650 , 550 , C_start_scale[1] , C_start_rot[1] , C_start[1] , TRUE , FALSE ) ;
		C_start_scale[1] += 0.02 ;
		C_start_rot[1]   -= 0.07  ;
		if( C_start_scale[1] >= 2.0f )
		{
			C_start_scale[1] = 2.0f ;
			C_start_rot[1] = 0.0f ;
		}
	}
	if( timer.d_time > 5 && timer.d_time <= 9 )
	{
		DrawRotaGraph( 960 , 450 , C_start_scale[2] , C_start_rot[2] , C_start[2] , TRUE , FALSE ) ;
		C_start_scale[2] += 0.02 ;
		if( C_start_scale[2] >= 2.0f )
		{
			C_start_scale[2] = 2.0f ;
		}
	}

	if( timer.d_time > 9 && timer.d_time <= 14 )
	{
		{
			static BOOL M_start = FALSE ;

			if ( M_start == FALSE ){
				M_start = TRUE ;
				PlaySoundMem( M_Game_Start_SE, DX_PLAYTYPE_BACK, TRUE ) ;
			}
		}
		DrawRotaGraph( 960 , 450 , C_startGo_scale , 0.0f , C_startGo , TRUE , FALSE ) ;
		C_startGo_scale += 0.2 ;
		if( C_startGo_scale >= 3.9f )
		{
			C_startGo_scale = 3.9f ;
		}
	}

	// --- 黒幕のxポジを足す
	if(kuromaku_flg != false){
		maku_pos_x -= maku_spd_x ;
	}

}

/* ===============================================================================
|
|   関数名	Maguma_Animation
|       処理内容  マグマのアニメーション
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Maguma_Animation()
{
	anim_playtime += anim_spd ;
	if( anim_totaltime  < anim_playtime )
		anim_playtime = 0.0f ;
}

/* ===============================================================================
|
|   関数名	DrawEffect
|       処理内容  エフェクトの描画
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void DrawEffect()
{
	VECTOR P1_pos , P2_pos ;

	P1_pos = m_Player[PLAYER1].char_pos ;
	P2_pos = m_Player[PLAYER2].char_pos ;

// ==============================================
//		1P エフェクトの描画
// ==============================================
	if( Atk_1pEfeFlg == true )
	{
		// ------------------------------------------
		//		キャラ : 右攻撃 →→→→→
		// ------------------------------------------
		if(m_Player[PLAYER1].char_rot == eRight)
		{
			P2_pos.x -= 50 ;
			P2_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 左攻撃 ←←←←←
		// ------------------------------------------
		if(m_Player[PLAYER1].char_rot == eLeft)
		{
			P2_pos.x += 50 ;
			P2_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 上攻撃 ↑↑↑↑↑
		// ------------------------------------------
		if(m_Player[PLAYER1].char_rot == eUp)
		{
			P2_pos.x += 5 ;
			P1_pos.z -= 5 ;
			P2_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 下攻撃 ↓↓↓↓↓
		// ------------------------------------------
		if(m_Player[PLAYER1].char_rot == eDown)
		{
			P2_pos.z += 30 ;
			P2_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.25f ;
			Atk_Scale.y += 0.25f ;
			Atk_Scale.z += 0.15f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 左上攻撃 ←↑←↑←
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 1.5f )
		{

			P2_pos.x += 5 ;
			P2_pos.y += 100 ;
			P2_pos.z -= 35 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;

			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 右上攻撃 →↑→↑→
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 2.5f )
		{
			P2_pos.x -= 5 ;
			P2_pos.y += 100 ;
			P2_pos.z -= 35 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 左下攻撃 ←↓←↓←
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 0.5f )
		{

			P2_pos.x -= 5 ;
			P2_pos.y += 100 ;
			P2_pos.z += 30 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.23f ;
			Atk_Scale.y += 0.23f ;
			Atk_Scale.z += 0.15f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		
		// ------------------------------------------
		//		キャラ : 右下攻撃 →↓→↓→
		// ------------------------------------------
		if ( m_Player[PLAYER1].char_rot == 3.5f )
		{
			P2_pos.x -= 5 ;
			P2_pos.y += 100 ;
			P2_pos.z += 30 ;
			MV1SetPosition(Atk_EffModel , P2_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.23f ;
			Atk_Scale.y += 0.20f ;
			Atk_Scale.z += 0.15f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}
	}

// ================================================
//		2P エフェクトの描画
// ================================================
	if( Atk_2pEfeFlg == true )
	{
		// ------------------------------------------
		//		キャラ : 右攻撃 →→→→→
		// ------------------------------------------
		if(m_Player[PLAYER2].char_rot == eRight)
		{
			P1_pos.x -= 50 ;
			P1_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 左攻撃 ←←←←←
		// ------------------------------------------
		if(m_Player[PLAYER2].char_rot == eLeft)
		{
			P1_pos.x += 50 ;
			P1_pos.y += 100 ;
			P1_pos.z -= 5 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 上攻撃 ↑↑↑↑↑
		// ------------------------------------------
		if(m_Player[PLAYER2].char_rot == eUp)
		{
			P1_pos.x += 5 ;
			P1_pos.z -= 5 ;
			P1_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 下攻撃 ↓↓↓↓↓
		// ------------------------------------------
		if(m_Player[PLAYER2].char_rot == eDown)
		{
			P1_pos.z += 30 ;
			P1_pos.y += 100 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.25f ;
			Atk_Scale.y += 0.25f ;
			Atk_Scale.z += 0.15f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 左上攻撃 ←↑←↑←
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 1.5f )
		{
			P1_pos.x += 5 ;
			P1_pos.y += 100 ;
			P1_pos.z -= 35 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 右上攻撃 →↑→↑→
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 2.5f )
		{
			P1_pos.x -= 5 ;
			P1_pos.y += 100 ;
			P1_pos.z -= 35 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.12f ;
			Atk_Scale.y += 0.12f ;
			Atk_Scale.z += 0.12f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 左下攻撃 ←↓←↓←
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 0.5f )
		{
			P1_pos.x -= 5 ;
			P1_pos.y += 100 ;
			P1_pos.z += 30 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.23f ;
			Atk_Scale.y += 0.23f ;
			Atk_Scale.z += 0.15f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}

		// ------------------------------------------
		//		キャラ : 右下攻撃 →↓→↓→
		// ------------------------------------------
		if ( m_Player[PLAYER2].char_rot == 3.5f )
		{
			P1_pos.x -= 10 ;
			P1_pos.y += 100 ;
			P1_pos.z += 30 ;
			MV1SetPosition(Atk_EffModel , P1_pos ) ;	
			MV1SetRotationXYZ( Atk_EffModel, VGet(0.0f, 1.57f * 1.0f , 0.0f ) ) ;
			Atk_Scale.x += 0.23f ;
			Atk_Scale.y += 0.20f ;
			Atk_Scale.z += 0.15f ;
			MV1SetScale( Atk_EffModel , Atk_Scale ) ;
			MV1DrawModel(Atk_EffModel) ;
			timer.e_time++ ;
		}
	}

	// --- 時間で消す
	if( timer.e_time >= 12 )
	{
		timer.e_time = 0 ;
		Atk_Scale = VGet( 1.1f , 1.1f , 1.1f) ;
		Atk_1pEfeFlg =false ;
		Atk_2pEfeFlg =false ;
	}
	
}

/* ===============================================================================
|
|   関数名	tennsuu
|       処理内容  点数管理
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void tennsuu( void )
{
	static int mototen[2] = {9,9} ;

	for ( int i = 0 ; i < 2 ; i++ ){
		if ( mototen[i] != p_score[i] ){
			purasu[i]-- ; 
			if ( p_score[i] %10 == 9 )
				purasu[i]++ ;
		}
		mototen[i] = p_score[i] ;
	}
//	printf( "%d\n", purasu[0] ) ;
}

/* ===============================================================================
|
|   関数名	DrawInit
|       処理内容  タイトルの初期セット
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void DrawInit()
{
	title_main = LoadGraph("..\\Data\\UI\\Title3.png") ;
	title_letter = LoadGraph("..\\Data\\UI\\GameStart.png") ;

	// --- 左右の旗持ち
	Title_Cheer[0] =  MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\1P_TitleFlag.mv1" ) ;
	Title_Cheer[1] =  MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\2P_TitleFlag.mv1" ) ;
	Title_Cheer[2] =  MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\3P_TitleFlag.mv1" ) ;

	for( int i = 0 ; i < 3 ; i++ )
	{
		Right_Title_Cheer[i] = MV1DuplicateModel(Title_Cheer[i]) ;
	}

	for ( int i = 0 ; i < PLAYER_MODEL ; i++ ){
		switch( i ){
			case 0 :
				t_Player[PLAYER1].player_model =  MV1LoadModel(PLAYER_MODEL_1) ;	
				t_Player[i].rootflm = MV1SearchFrame(t_Player[i].player_model,"obj_2") ;								// --- ボーンの番号を返す
				t_Player[i].p_Model = t_Player[i].player_model ;														// --- モデル情報格納
				break ;
			case 1 :
				t_Player[PLAYER2].player_model =  MV1LoadModel(PLAYER_MODEL_2) ;	
				t_Player[i].rootflm = MV1SearchFrame(t_Player[i].player_model,"obj_2") ;								// --- ボーンの番号を返す
				t_Player[i].p_Model = t_Player[i].player_model ;														// --- モデル情報格納
				break ;
			case 2 :
				t_Player[PLAYER3].player_model =  MV1LoadModel(PLAYER_MODEL_3) ;	
				t_Player[i].rootflm = MV1SearchFrame(t_Player[i].player_model,"obj_2") ;								// --- ボーンの番号を返す
				t_Player[i].p_Model = t_Player[i].player_model ;														// --- モデル情報格納
				break ;
		}
	}

	
	// --- タイトル アニメーション読み込み
	title_Flager[0].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\1P_TitleFlag_anim.mv1" ) ;
	title_Flager[1].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\2P_TitleFlag_anim.mv1" ) ;
	title_Flager[2].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\3P_TitleFlag_anim.mv1" ) ;

	Right_title_Flager[0].anim_nutral =  MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\1P_TitleFlag_anim.mv1" ) ;
	Right_title_Flager[1].anim_nutral =  MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\2P_TitleFlag_anim.mv1" ) ;
	Right_title_Flager[2].anim_nutral =  MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\3P_TitleFlag_anim.mv1" ) ;
	
	// --- 応援団の位置
	Title_Cheer_pos[0] =  VGet( -700.0f , 250.0f , 450.0f ) ;		
	Title_Cheer_pos[1] =  VGet( -700.0f , 250.0f , 1100.0f ) ;		
	Title_Cheer_pos[2] =  VGet( -500.0f , 250.0f , 1750.0f ) ;

	Right_Title_Cheer_pos[0] =  VGet( 1500.0f , 250.0f , 450.0f ) ;		
	Right_Title_Cheer_pos[1] =  VGet( 1500.0f , 250.0f , 1200.0f ) ;		
	Right_Title_Cheer_pos[2] =  VGet( 1200.0f , 250.0f , 1850.0f ) ;

	title_Flager[0].anim_playtime = 10.0f ;

	// --- 旗持ちのアニメーション
	for( int i = 0 ; i < 3 ; i++ )
	{
		title_Flager[i].tmp_attachidx  = MV1AttachAnim(Title_Cheer[i] , 0 , title_Flager[i].anim_nutral ) ;			// --- モデルにアタッチ
		title_Flager[i].anim_totaltime = MV1GetAttachAnimTotalTime(Title_Cheer[i], title_Flager[i].tmp_attachidx) ;	// --- アニメーション総時間
		title_Flager[i].anim_spd = 1.0f ;

		Right_title_Flager[i].tmp_attachidx  = MV1AttachAnim(Right_Title_Cheer[i] , 0 , Right_title_Flager[i].anim_nutral ) ;			// --- モデルにアタッチ
		Right_title_Flager[i].anim_totaltime = MV1GetAttachAnimTotalTime(Right_Title_Cheer[i], Right_title_Flager[i].tmp_attachidx) ;	// --- アニメーション総時間
		Right_title_Flager[i].anim_spd = 1.0f ;

	}

	t_Player[PLAYER1].char_pos =  VGet( 200.0f , 300.0f , -250.0f ) ;		// プレイヤー1の初期座標
	t_Player[PLAYER2].char_pos =  VGet( 600.0f , 300.0f , -250.0f ) ;		// プレイヤー2の初期座標
	t_Player[PLAYER3].char_pos =  VGet( 400.0f , 300.0f , -50.0f ) ;		// プレイヤー3の初期座標

	// プレイヤー1の初期アニメーションセット
	t_Player[PLAYER1].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Stay.mv1" ) ;	// --- 静止アニメーション
	t_Player[PLAYER1].tmp_attachidx = MV1AttachAnim(t_Player[PLAYER1].player_model,0,t_Player[PLAYER1].anim_nutral) ;						// --- モデルにアタッチ
	t_Player[PLAYER1].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[PLAYER1].player_model,t_Player[PLAYER1].tmp_attachidx) ;		// --- アニメーション総時間取得

	// プレイヤー2の初期アニメーションセット
	t_Player[PLAYER2].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Stay.mv1" ) ;	// --- 静止アニメーション
	t_Player[PLAYER2].tmp_attachidx = MV1AttachAnim(t_Player[PLAYER2].player_model,0,t_Player[PLAYER2].anim_nutral) ;						// --- モデルにアタッチ
	t_Player[PLAYER2].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[PLAYER2].player_model,t_Player[PLAYER2].tmp_attachidx) ;		// --- アニメーション総時間取得

	// プレイヤー3の初期アニメーションセット
	t_Player[PLAYER3].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Stay.mv1" ) ;	// --- 静止アニメーション
	t_Player[PLAYER3].tmp_attachidx = MV1AttachAnim(t_Player[PLAYER3].player_model,0,t_Player[PLAYER3].anim_nutral) ;						// --- モデルにアタッチ
	t_Player[PLAYER3].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[PLAYER3].player_model,t_Player[PLAYER3].tmp_attachidx) ;		// --- アニメーション総時間取得

	t_Player[PLAYER1].anim_spd = 2.0f ;
	t_Player[PLAYER2].anim_spd = 2.0f ;
	t_Player[PLAYER3].anim_spd = 2.0f ;


	// --- Playerの初期アニメーション（立ち）
	t_Player[PLAYER1].animflg = pStand ;
	t_Player[PLAYER2].animflg = pStand ;
	t_Player[PLAYER3].animflg = pStand ;

}

/* ===============================================================================
|
|   関数名	DrawTitle
|       処理内容  タイトルの描画と処理
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void DrawTitle ()
{

	DrawBox(0,0,1920,1080,GetColor(200,200,255),true) ; //最後の引数をfalseにすると塗りつぶし無し

	// --- 背景などのオブジェクトの描画
	// --- ステージの大きさを変更

	MV1SetAttachAnimTime( maguma_model , tmp_attachidx , anim_playtime) ;

	MV1SetRotationXYZ( Model[5] , VGet(0.0f, 1.57f * 0.2f, 0.0f) ) ;
	MV1SetRotationXYZ( Model[6] , VGet(0.0f, 1.57f * 1.8f, 0.0f) ) ;

//	printf( "%f\n", anim_totaltime ) ;
	for( int i = 0 ; i < MAX_OBJ ; i++ )
	{
		MV1SetPosition(Model[i] , pos[i] ) ;
		MV1DrawModel(Model[i]) ;
	}

		// --- 旗持ちの描画
	for( int i = 0 ; i < 3 ; i++ )
	{
		MV1SetScale( Title_Cheer[i] , VGet( 1.0f , 1.0f , 1.0f) ) ;
		MV1SetAttachAnimTime(Title_Cheer[i], title_Flager[i].tmp_attachidx  ,title_Flager[i].anim_playtime) ;
		MV1SetPosition(Title_Cheer[i] , Title_Cheer_pos[i] ) ;
		MV1DrawModel(Title_Cheer[i]) ;

		MV1SetScale( Right_Title_Cheer[i] , VGet( 0.9f , 0.9f , 0.9f) ) ;
		MV1SetAttachAnimTime(Right_Title_Cheer[i], Right_title_Flager[i].tmp_attachidx  , Right_title_Flager[i].anim_playtime ) ;
		MV1SetRotationXYZ( Right_Title_Cheer[i], VGet(0.0f, 1.57f * 2.0f , 0.0f ) ) ;
		MV1SetPosition(Right_Title_Cheer[i] , Right_Title_Cheer_pos[i] ) ;
		MV1DrawModel(Right_Title_Cheer[i]) ;
	}

	// --- アニメーション
	timer.Title_Flag_Timer() ;	// --- 時間計測
	
	title_Flager[0].anim_playtime  += title_Flager[0].anim_spd ; // 左前
	Right_title_Flager[0].anim_playtime  += Right_title_Flager[0].anim_spd ; // 右前
	
	if( timer.tFlag_time >= 1 )
	{
		title_Flager[1].anim_playtime  += title_Flager[1].anim_spd ; // 左真ん中	
		Right_title_Flager[1].anim_playtime  += Right_title_Flager[1].anim_spd ; // 右真ん中
	}
	if( timer.tFlag_time >= 2 )
	{
		title_Flager[2].anim_playtime  += title_Flager[2].anim_spd ; // 左後
		Right_title_Flager[2].anim_playtime  += Right_title_Flager[2].anim_spd ; // 右後
	}

	for( int i = 0 ; i < 3 ; i++ )
	{
		if( title_Flager[i].anim_totaltime  < title_Flager[i].anim_playtime )
			title_Flager[i].anim_playtime = 0.0f ;
		if(Right_title_Flager[i].anim_totaltime  < Right_title_Flager[i].anim_playtime) 
			Right_title_Flager[i].anim_playtime = 0.0f ;

	}

	// --- プレイヤーの描画
	for( int i = 0 ; i < PLAYER_MODEL ; i++ )
	{
		MV1SetScale( t_Player[i].p_Model , VGet( 0.6f , 0.6f , 0.6f) ) ;
		MV1SetAttachAnimTime(t_Player[i].p_Model, t_Player[i].tmp_attachidx  ,t_Player[i].anim_playtime) ;		// アニメーションの何番目（何秒後）の動きか同期させる
		t_Player[i].SetMovePosition() ;
		MV1SetRotationXYZ( t_Player[i].p_Model, VGet(0.0f, 1.57f * t_Player[i].char_rot , 0.0f ) ) ;
		MV1SetPosition(t_Player[i].p_Model , t_Player[i].char_pos ) ;	
		MV1DrawModel(t_Player[i].p_Model) ;

		// --- プレイヤー影付与
		MV1SetScale( shadow_p , VGet( 0.6f , 0.6f , 0.6f) ) ;
		MV1SetRotationXYZ( shadow_p, VGet(0.0f, 1.57f * t_Player[i].char_rot , 0.0f ) ) ;
		MV1SetPosition(shadow_p , VGet( t_Player[i].char_pos.x , 275.0f , (t_Player[i].char_pos.z-10)) ) ;
		MV1DrawModel(shadow_p) ;
	}

	// --- マグマのアニメーション
	Maguma_Animation() ;	

	// --- メインタイトル
	DrawGraph(-50, 30, title_main, true) ;

	// --- タイトルstartの点滅
	if( timer.t_time % 90 < 40 && sceneFlg == 0 )
	{
		DrawGraph(470, 750, title_letter, true) ;
	}

	if( sceneFlg == 1 )
	{	
		timer.t_time++ ;

		if( timer.t_time % 18< 10 )
		{
			DrawGraph(470, 750, title_letter, true) ;
		}
//		printf( "time = %d" , timer.t_time) ;
	}
}

/* ===============================================================================
|
|   関数名	Draw_Cselect_Init
|       処理内容  キャラ選択画面の描画と処理
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Draw_Cselect_Init()
{
	m_Player[PLAYER1].ready_check = false ;
	m_Player[PLAYER2].ready_check = false ;

	m_Player[PLAYER1].p_flg = 0 ;
	m_Player[PLAYER1].sp_flg = 0 ;
	m_Player[PLAYER2].p_flg = 0 ;
	m_Player[PLAYER2].sp_flg = 0 ;

	// --- AYRの座標と移動量
	AYR_pos[0] = -960  ;
	AYR_pos[1] =  1920 ;
	AYR_spd    = 30 ;

	// --- 背景
	cSelect_BG = LoadGraph( "..\\Data\\UI\\cSelect_BG.png" ) ;

	// --- 矢印 白矢印 カーソル
	for( int i = 0 ; i < 2 ; i++ )
	{
		left_arrow[i]  = LoadGraph("..\\Data\\UI\\Left_Arrow.png") ;
		right_arrow[i] = LoadGraph("..\\Data\\UI\\Right_Arrow.png") ;
		left_white_arrow[i]  = LoadGraph("..\\Data\\UI\\Left_Arrow_White.png")  ;
		right_white_arrow[i] = LoadGraph("..\\Data\\UI\\Right_Arrow_White.png") ;
		Select_cursor[i]     = LoadGraph("..\\Data\\UI\\Select_Cursor.png") ;

		cSelect_Face_BG[i] = LoadGraph( "..\\Data\\UI\\sele_cha_bg.png" ) ;

	}
	// --- 上の顔
	Select_face[PLAYER1]  = LoadGraph("..\\Data\\UI\\1P_face.png") ;
	Select_face[PLAYER2]  = LoadGraph("..\\Data\\UI\\2P_face.png") ;
	Select_face[PLAYER3]  = LoadGraph("..\\Data\\UI\\3P_face.png") ;
	
	ready_letter  = LoadGraph( "..\\Data\\UI\\ready.png" ) ;
	LoadDivGraph("..\\Data\\UI\\Are_You_Ready.png" , 2 , 2 , 1 , 960 , 270 , AYR_ui) ;
}

/* ===============================================================================
|
|   関数名	Draw_Cselect
|       処理内容  キャラ選択画面の描画と処理
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void Draw_Cselect()
{
	int  select_yazi[2] = {0,0} ;

	DrawBox(0,0,1920,1080,GetColor(56, 8 ,120),true) ; //最後の引数をfalseにすると塗りつぶし無し

	// --- 背景
	DrawGraph( -80  , -50 , cSelect_BG, true) ;

	// --- 上の顔の背景
	DrawGraph( 50  , 35 , cSelect_Face_BG[0] , true ) ;
	DrawGraph( 960 , 35 , cSelect_Face_BG[1] , true ) ;

	// --- 押されるまで回転と移動を繰り返す
    if(m_Player[PLAYER1].ready_check == false )
	{
	 	P1_Rot_spd      = -0.05f ;
		P_rot[PLAYER1] += P1_Rot_spd ;

		m_Player[PLAYER1].select_C_pos.y += p1_Move_spd ;

		if( m_Player[PLAYER1].select_C_pos.y > 260 )
			p1_Move_spd -= 0.8f ;
		if( m_Player[PLAYER1].select_C_pos.y == 260 )
			p1_Move_spd = 6.0f ;
		//if( m_Player[PLAYER1].select_C_pos.y < 330 )
		//	p1_Move_spd += 0.8f ;
	}

	if( m_Player[PLAYER2].ready_check == false )
	{
		P2_Rot_spd      =  0.05f ;
		P_rot[PLAYER2] += P2_Rot_spd ;

		m_Player[PLAYER2].select_C_pos.y += p2_Move_spd ;

		if( m_Player[PLAYER2].select_C_pos.y > 260 )
			p2_Move_spd -= 0.8f ;
		if( m_Player[PLAYER2].select_C_pos.y == 260 )
		    p2_Move_spd = 6.0f ;
		//if( m_Player[PLAYER2].select_C_pos.y < 330 )
		//	p2_Move_spd += 0.8f ;
	}

	// --- 顔とセレクトカーソルの描画描画
	// P1
	if(m_Player[PLAYER1].p_flg == PLAYER1)
	{			
		DrawGraph(390  , 85 , Select_face[PLAYER1] , true) ;
		DrawExtendGraph(  650, 150,  820, 270,  Select_face[PLAYER2] , true ) ;
		DrawExtendGraph(  220, 140,  370, 270,  Select_face[PLAYER3] , true ) ;

		DrawGraph(405 , 100 , Select_cursor[0] , true ) ;
	}
	if(m_Player[PLAYER1].p_flg == PLAYER2)
	{
		DrawGraph(610  , 100 , Select_face[PLAYER2] , true) ;
		DrawExtendGraph(  410, 150,  580, 275,  Select_face[PLAYER1] , true ) ;
		DrawExtendGraph(  220, 140,  370, 270,  Select_face[PLAYER3] , true ) ;

		DrawGraph(620 , 100 , Select_cursor[0] , true ) ;
	}
	if(m_Player[PLAYER1].p_flg == PLAYER3)
	{
		DrawGraph(150  , 90 , Select_face[PLAYER3] , true) ;
		DrawExtendGraph(  430, 150,  610, 270,  Select_face[PLAYER1] , true ) ;
		DrawExtendGraph(  650, 150,  820, 270,  Select_face[PLAYER2] , true ) ;

		DrawGraph(170 , 100 , Select_cursor[0] , true ) ;
	}

	// P2
	if(m_Player[PLAYER2].p_flg == PLAYER1)
	{
		DrawGraph(1300  , 85 , Select_face[PLAYER1] , true) ;
		DrawExtendGraph(  1560, 150,  1730, 270,  Select_face[PLAYER2] , true ) ;
		DrawExtendGraph(  1120, 140,  1270, 270,  Select_face[PLAYER3] , true ) ;

		DrawGraph(1315 , 100 , Select_cursor[1] , true ) ;
	}
	if(m_Player[PLAYER2].p_flg == PLAYER2)
	{
		DrawGraph(1540 , 100 , Select_face[PLAYER2] , true) ;
		DrawExtendGraph(  1330, 150,  1500, 275,  Select_face[PLAYER1] , true ) ;
		DrawExtendGraph(  1120, 140,  1270, 270,  Select_face[PLAYER3] , true ) ;

		DrawGraph(1550 , 100 , Select_cursor[1] , true ) ;
	}
	if(m_Player[PLAYER2].p_flg == PLAYER3)
	{
		DrawGraph(1080 , 90 , Select_face[PLAYER3] , true) ;
		DrawExtendGraph(  1350, 150,  1520, 270,  Select_face[PLAYER1] , true ) ;
		DrawExtendGraph(  1560, 150,  1730, 270,  Select_face[PLAYER2] , true ) ;

		DrawGraph(1100 , 100 , Select_cursor[1] , true ) ;	
	}

	// --- プレイヤーの描画
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		switch( m_Player[i].p_flg ){

			case PLAYER1 :
				MV1SetRotationXYZ( m_Player[i].pSelect_Model[0], VGet(0.0f, P_rot[i] , 0.0f ) ) ;
				MV1SetScale( m_Player[i].pSelect_Model[0] , VGet( 1.0f , 1.0f , 1.0f) ) ;
				MV1SetPosition(m_Player[i].pSelect_Model[0] , m_Player[i].select_C_pos ) ;	
				MV1DrawModel(m_Player[i].pSelect_Model[0]) ;
				break ;

			case PLAYER2 :
				MV1SetRotationXYZ( m_Player[i].pSelect_Model[1], VGet(0.0f, P_rot[i] , 0.0f ) ) ;
				MV1SetScale( m_Player[i].pSelect_Model[1] , VGet( 1.0f , 1.0f , 1.0f) ) ;
				MV1SetPosition(m_Player[i].pSelect_Model[1] , m_Player[i].select_C_pos ) ;	
				MV1DrawModel(m_Player[i].pSelect_Model[1]) ;
				break ;

			case PLAYER3 :
				MV1SetRotationXYZ( m_Player[i].pSelect_Model[2], VGet(0.0f, P_rot[i] , 0.0f ) ) ;
				MV1SetScale( m_Player[i].pSelect_Model[2] , VGet( 1.0f , 1.0f , 1.0f) ) ;
				MV1SetPosition(m_Player[i].pSelect_Model[2] , m_Player[i].select_C_pos ) ;	
				MV1DrawModel(m_Player[i].pSelect_Model[2]) ;
				break ;
		}
	}

	// --- 〇が押されたら回転と移動の停止
	if (m_Player[PLAYER1].ready_check == true && Spinflg == 0 )
	{
		DrawGraph(300 , 550, ready_letter, true) ;					// --- Ready画像の描画
		P_rot[PLAYER1] = P1_Rot_spd ;
		P1_Rot_spd = -0.3f ;
	}
	if (m_Player[PLAYER2].ready_check == true && Spinflg == 0 )
	{
		DrawGraph(1210, 550, ready_letter, true) ;					// --- Ready画像の描画
		P_rot[PLAYER2] = P2_Rot_spd ;
		P2_Rot_spd = 0.3f ;
		
	}

	// --- 両方〇が押されたらフラグを立てる
	if (m_Player[PLAYER1].ready_check == true && 
		m_Player[PLAYER2].ready_check == true){
			if(timer.s_spin_time > 50){
				Spinflg = true ;
			}
	}

	// --- フラグが立ったらキャラ回転＋上昇
	if(Spinflg == true){
		P_rot[PLAYER1] += P1_Rot_spd ;
		P1_Rot_spd += -0.002f ;
		P_rot[PLAYER2] += P2_Rot_spd ;
		P2_Rot_spd += 0.002f ;
		if(P2_Rot_spd >= 0.5){
			m_Player[PLAYER1].select_C_pos.y += 10.0f ;
			m_Player[PLAYER2].select_C_pos.y += 10.0f ;
		}
	}

	
	// --- 白い矢印の描画
	for ( int i = 0 ; i < MAX_PLAYER ; i++ )
	{
		// --- 左右キーが押されたとき
		if ( m_Player[i].sp_flg == 1 ){
			if( g_padstate[PLAYER1].input & PAD_INPUT_LEFT && m_Player[i].ready_check == false){
				select_yazi[PLAYER1] = 1 ;
				DrawGraph(90  , 590 , left_white_arrow[0], true) ;			// --- 1Pの左矢印
			}
			if( g_padstate[PLAYER1].input & PAD_INPUT_RIGHT && m_Player[i].ready_check == false ){
				select_yazi[PLAYER1] = 2 ;
				DrawGraph(790 , 590 , right_white_arrow[0], true) ;			// --- 1Pの右矢印
			}
			if( g_padstate[PLAYER2].input & PAD_INPUT_LEFT && m_Player[i].ready_check == false){
				select_yazi[PLAYER2] = 1 ;
				DrawGraph(1000 , 590 , left_white_arrow[1], true) ;			// --- 2Pの左矢印
			}
			if( g_padstate[PLAYER2].input & PAD_INPUT_RIGHT && m_Player[i].ready_check == false){
				select_yazi[PLAYER2] = 2 ;
				DrawGraph(1690 , 590 , right_white_arrow[1], true) ;		// --- 2Pの右矢印
			}
		}
	}

	// --- 矢印の描画 (白矢印の時は消す)
	if( select_yazi[PLAYER1] != 1 ){
		DrawGraph(90  , 580 , left_arrow[0], true) ;
	}
	if( select_yazi[PLAYER1] != 2 ){
		DrawGraph(790 , 580 , right_arrow[0], true) ;
	}
	if( select_yazi[PLAYER2] != 1 ){
		DrawGraph(1000 , 580 , left_arrow[1], true) ;
	}
	if( select_yazi[PLAYER2] != 2 ){
		DrawGraph(1690 , 580 , right_arrow[1], true) ;
	}

	// --- AreYouReady
	DrawGraph(AYR_pos[0] , 500 , AYR_ui[0], true) ;
	DrawGraph(AYR_pos[1] , 500 , AYR_ui[1], true) ;

	if( m_Player[PLAYER1].ready_check == true && 
		m_Player[PLAYER2].ready_check == true ){

 		AYR_pos[0] += AYR_spd ;
		AYR_pos[1] -= AYR_spd ;

		if( AYR_pos[0] >=0 )
		{
			AYR_pos[0] = 0   ;
			AYR_pos[1] = 960 ;
		}
	}

//	printf("%d\n" , m_Player[0].p_flg) ;

}

/* ===============================================================================
|
|   関数名	DrawResult_Init
|       処理内容  リザルト画面の初期セット
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void DrawResult_Init()
{
	// --- 台の位置
	es_pos[0] = VGet(0.0f , -100.0f , 50.0f) ;	// 左
	es_pos[1] = VGet(800.0f , -100.0f , 50.0f) ;	// 右

	// --- キャラの位置
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		t_Player[i].char_pos =  VGet( es_pos[i].x , (es_pos[i].y+100) , es_pos[i].z ) ;		// プレイヤーの初期座標
	}

	// --- 背景画
//	End_BG = LoadGraph( "..\\Data\\UI\\cSelect_BG.png" ) ;
	End_BG = LoadGraph("..\\Data\\UI\\Judgement1.png") ;										// --- judgementの文字
	End_Effect[0] = LoadGraph( "..\\Data\\UI\\Result_Effect.png" ) ;							// --- 右側から出るほう
	End_Effect[1] = LoadGraph( "..\\Data\\UI\\Result_Effect2.png" ) ;							// --- 左側から出るほう
	LoadDivGraph( "..\\Data\\UI\\back_effect.png" , 5 , 5 , 1 , 384 , 600 ,End_Buck_Effect) ;	// --- 落ちてくるエフェクト

	if ( m_Player[0].p_flg == 0 ){
		if( m_Player[1].p_flg == 0 ){
			Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_RvsR.png" ) ;
		}
		else{
			if( m_Player[1].p_flg == 1 ){
				Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_RvsB.png" ) ;
			}
			else{
				Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_RvsG.png" ) ;
			}
		}
	}
	else{
		if ( m_Player[0].p_flg == 1 ){
			if( m_Player[1].p_flg == 0 ){
				Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_BvsR.png" ) ;
			}
			else{
				if( m_Player[1].p_flg == 1 ){
					Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_BvsB.png" ) ;
				}
				else{
					Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_BvsG.png" ) ;
				}
			}
		}
		else{
			if ( m_Player[0].p_flg == 2 ){
				if( m_Player[1].p_flg == 0 ){
					Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_GvsR.png" ) ;
				}
				else{
					if( m_Player[1].p_flg == 1 ){
						Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_GvsB.png" ) ;
					}
					else{
						Result_BG = LoadGraph( "..\\Data\\BackGround\\Re_BG_GvsG.png" ) ;
					}
				}
			}
		}
	}

	// --- win/lose
	Win_model  = MV1LoadModel("..\\Data\\BackGround\\Win.mv1") ;
	Lose_model = MV1LoadModel("..\\Data\\BackGround\\Lose.mv1") ;

	// --- モデルの読み込み
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		switch( m_Player[i].p_flg ){
			case 0 :
				t_Player[i].player_model =  MV1LoadModel(PLAYER_MODEL_1) ;	
				t_Player[i].rootflm = MV1SearchFrame(t_Player[i].player_model,"obj_2") ;								// --- ボーンの番号を返す
				t_Player[i].p_Model = t_Player[i].player_model ;														// --- モデル情報格納
				End_Stage[i] = MV1LoadModel("..\\Data\\Stage\\M_1P_ResultStage.mv1") ;									// --- 台のモデルの読み込み

				// --- プレイヤー1の敗北（1）アニメーション
				t_Player[i].anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_lose[0]) ;						// --- モデルにアタッチ
				result_anim_cnt = t_Player[i].tmp_attachidx ;
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// --- プレイヤー1の敗北（2）アニメーション
				t_Player[i].anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_lose[1]) ;						// --- モデルにアタッチ
				result_anim_cnt = t_Player[i].tmp_attachidx ;
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// --- プレイヤー1の勝利アニメーション
				t_Player[i].anim_win = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Happy.mv1" ) ;								// --- 勝利アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_win) ;						// --- モデルにアタッチ
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// プレイヤー1の初期アニメーションセット
				t_Player[i].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_1P\\mv1\\M_1P_Stay.mv1" ) ;							// --- 静止アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_nutral) ;						// --- モデルにアタッチ
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				t_Player[i].anim_spd = 2.0f ;
				// --- Playerの初期アニメーション（立ち）
				t_Player[i].animflg = pStand ;
				break ;

			case 1 :
				t_Player[i].player_model =  MV1LoadModel(PLAYER_MODEL_2) ;	
				t_Player[i].rootflm = MV1SearchFrame(t_Player[i].player_model,"obj_2") ;								// --- ボーンの番号を返す
				t_Player[i].p_Model = t_Player[i].player_model ;														// --- モデル情報格納
				End_Stage[i] = MV1LoadModel("..\\Data\\Stage\\M_2P_ResultStage.mv1") ;									// --- 台のモデルの読み込み

				// --- プレイヤー1の敗北（1）アニメーション
				t_Player[i].anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_lose[0]) ;						// --- モデルにアタッチ
				result_anim_cnt = t_Player[i].tmp_attachidx ;
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// --- プレイヤー1の敗北（2）アニメーション
				t_Player[i].anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_lose[1]) ;						// --- モデルにアタッチ
				result_anim_cnt = t_Player[i].tmp_attachidx ;
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// --- プレイヤー1の勝利アニメーション
				t_Player[i].anim_win = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Happy.mv1" ) ;		// --- 勝利アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_win) ;						// --- モデルにアタッチ
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// プレイヤー1の初期アニメーションセット
				t_Player[i].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_2P\\mv1\\M_2P_Stay.mv1" ) ;							// --- 静止アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_nutral) ;						// --- モデルにアタッチ
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				t_Player[i].anim_spd = 2.0f ;
				// --- Playerの初期アニメーション（立ち）
				t_Player[i].animflg = pStand ;
				break ;

			case 2 :
				t_Player[i].player_model =  MV1LoadModel(PLAYER_MODEL_3) ;	
				t_Player[i].rootflm = MV1SearchFrame(t_Player[i].player_model,"obj_2") ;								// --- ボーンの番号を返す
				t_Player[i].p_Model = t_Player[i].player_model ;														// --- モデル情報格納
				End_Stage[i] = MV1LoadModel("..\\Data\\Stage\\M_3P_ResultStage.mv1") ;									// --- 台のモデルの読み込み
	
				// --- プレイヤー1の敗北（1）アニメーション
				t_Player[i].anim_lose[0] = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Sad_1.mv1" ) ;							// --- 敗北アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_lose[0]) ;						// --- モデルにアタッチ
				result_anim_cnt = t_Player[i].tmp_attachidx ;
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// --- プレイヤー1の敗北（2）アニメーション
				t_Player[i].anim_lose[1] = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Sad_2.mv1" ) ;							// --- 敗北アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_lose[1]) ;						// --- モデルにアタッチ
				result_anim_cnt = t_Player[i].tmp_attachidx ;
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// --- プレイヤー1の勝利アニメーション
				t_Player[i].anim_win = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Happy.mv1" ) ;								// --- 勝利アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_win) ;						// --- モデルにアタッチ
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				MV1DetachAnim(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;										// --- アニメーションをデタッチ

				// プレイヤー1の初期アニメーションセット
				t_Player[i].anim_nutral = MV1LoadModel( "..\\Data\\Player\\M_3P\\mv1\\M_3P_Stay.mv1" ) ;							// --- 静止アニメーション
				t_Player[i].tmp_attachidx = MV1AttachAnim(t_Player[i].player_model,0,t_Player[i].anim_nutral) ;						// --- モデルにアタッチ
				t_Player[i].anim_totaltime = MV1GetAttachAnimTotalTime(t_Player[i].player_model,t_Player[i].tmp_attachidx) ;		// --- アニメーション総時間取得
				t_Player[i].anim_spd = 2.0f ;
				// --- Playerの初期アニメーション（立ち）
				t_Player[i].animflg = pStand ;
				break ;
		}
	}

}

/* ===============================================================================
|
|   関数名	DrawResult
|       処理内容  リザルト画面の描画と処理
+ --------------------------------------------------------------------------------
|   IN --->            --> 
|   OUT -->            --> 
+ ============================================================================= */
void DrawResult()
{
	int flg = 0 ;										// 0 == 台とキャラの上下中 / 1 == 勝敗前の演出に切り替え 
	int change_effect_flg = 0 ;							// 0 == 左右から出るエフェクト　/　1 == 上から流れてくるエフェクト

	// --- 背景
	DrawGraph( 0  , 0 , Result_BG, true) ;				// 背景画
	DrawGraph( 500 , 10 , End_BG , true) ;				// 文字

	{
		static BOOL anim_f = FALSE ;

		// --- リザルト開始から5秒後
		if(timer.re_time >= 5 && anim_f == FALSE){
			anim_f = TRUE ;
			if(	m_Player[PLAYER1].death_count > m_Player[PLAYER2].death_count )						// --- 落ちた回数が少ないほうが勝ち(2P)
			{
				sp_scale[PLAYER2] = 0.7f;
				char_pos_spd[PLAYER2] = 41.0f;
				t_Player[PLAYER1].animflg = pLose_S ; 			// --- Playerのアニメーション（悔しがり）
			}
			else if( m_Player[PLAYER2].death_count > m_Player[PLAYER1].death_count )				// --- 落ちた回数が少ないほうが勝ち(1P)
			{
				sp_scale[PLAYER1] = 0.7f;
				char_pos_spd[PLAYER1] = 41.0f;
				t_Player[PLAYER2].animflg = pLose_S ; 			// --- Playerのアニメーション（悔しがり）
			}
			else if ( m_Player[PLAYER1].death_count == m_Player[PLAYER2].death_count )				// --- 落ちた回数が同じなら
			{
				if ( m_Player[PLAYER1].press_count < m_Player[PLAYER2].press_count )				// --- 潰された回数が少ないほうの勝ち(1P勝ち)
				{
					sp_scale[PLAYER1] = 0.7f;
					char_pos_spd[PLAYER1] = 41.0f;
					t_Player[PLAYER2].animflg = pLose_S ; 		// --- Playerのアニメーション（悔しがり）
				}
				else if ( m_Player[PLAYER2].press_count < m_Player[PLAYER1].press_count )			// --- 潰された回数が少ないほうの勝ち(2P勝ち)
				{
					sp_scale[PLAYER2] = 0.7f;
					char_pos_spd[PLAYER2] = 41.0f;
					t_Player[PLAYER1].animflg = pLose_S ; 		// --- Playerのアニメーション（悔しがり）
				}
				else if ( m_Player[PLAYER1].press_count == m_Player[PLAYER2].press_count )			// --- 潰された回数も同じだったら
				{
					if ( m_Player[PLAYER1].attack_count < m_Player[PLAYER2].attack_count )			// --- 攻撃が成功した回数が多いほうが勝ち(2P勝ち)
					{

						sp_scale[PLAYER2] = 0.7f;
						char_pos_spd[PLAYER2] = 41.0f;
						t_Player[PLAYER1].animflg = pLose_S ; 	// --- Playerのアニメーション（悔しがり）
					}
					else if ( m_Player[PLAYER2].attack_count < m_Player[PLAYER1].attack_count )		// --- 攻撃が成功した回数が多いほうが勝ち(1P勝ち)
					{	
						sp_scale[PLAYER1] = 0.7f;
						char_pos_spd[PLAYER1] = 41.0f;
						t_Player[PLAYER2].animflg = pLose_S ; 	// --- Playerのアニメーション（悔しがり）
					}
					else if ( m_Player[PLAYER2].attack_count == m_Player[PLAYER1].attack_count )
					{
						if ( m_Player[PLAYER1].lastfall_flg == TRUE )								// --- 最後に落下したほうが負け(1P負け)
						{
							sp_scale[PLAYER2] = 0.7f;
							char_pos_spd[PLAYER2] = 41.0f;
							t_Player[PLAYER1].animflg = pLose_S ; 	// --- Playerのアニメーション（悔しがり）
						} 
						else if ( m_Player[PLAYER2].lastfall_flg == TRUE )							// --- 最後に落下したほうが負け(2P負け)
						{
							sp_scale[PLAYER1] = 0.7f;
							char_pos_spd[PLAYER1] = 41.0f;
							t_Player[PLAYER2].animflg = pLose_S ; 	// --- Playerのアニメーション（悔しがり）
						}
					}
				}
			}
		}
	}

	// --- 虹が通り過ぎたら処理開始 ----------------------------------------------------------------------------------------------//
	if(maku_pos_x <= -2620){
		// --- スケールの反復
		for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
			// --- 上下に移動した後の動き（poscntで上下移動の回数指定）
			if(poscnt >= 5){ 
				// --- 同じようなところで止まる　End_Stage_Scale[i] <= 10.0f && End_Stage_Scale[i] >= 9.5f 
				if(End_Stage_Scale[0] == End_Stage_Scale[1] && timer.re_time < 4 && flg == 0){
					sp_scale[0] = 0.0f;
					char_pos_spd[0] = 0.0f;
					sp_scale[1] = 0.0f;
					char_pos_spd[1] = 0.0f;
					flg = 1 ;
					// --- どちらも一度下に降ろすために値をセット
					if( flg != 0 ){
						sp_scale[0] = -0.4f;
						char_pos_spd[0] = -24.0f;
						sp_scale[1] = -0.4f;
						char_pos_spd[1] = -24.0f;
					}
				}
			}

			// --- 負けアニメーションのループするアニメーションへの切り替え
			if ( (t_Player[i].animflg == pLose_S) && (t_Player[i].anim_totaltime <= t_Player[i].anim_playtime) ){
				t_Player[i].animflg = pLose ;
			}


			// --- 台が上に伸び切った時
			if(End_Stage_Scale[i] >= 8.0f && sp_scale[i] > 0){
				 sp_scale[i] *= -1.0f;									// --- 台の伸び縮みの切り替え
				 char_pos_spd[i] *= -1.0f;								// --- キャラの上下に動くスピードの切り替え
				 // --- 台が動き始めてから5秒後に上に伸び切った台とキャラの停止
				 if(timer.re_time >= 5){
					 sp_scale[i] = 0.0f;
					 char_pos_spd[i] = 0.0f; 
					 // --- Playerのアニメーション（喜び）
					 t_Player[i].animflg = pWin ;
   				 }
			}
			// --- 台が下に降り切った時
			if(End_Stage_Scale[i] <= 1.0f && sp_scale[i] < 0){
				sp_scale[i] *= -1.0f;									// --- 台の伸び縮みの切り替え
				char_pos_spd[i] *= -1.0f;								// --- キャラの上下に動くスピードの切り替え
				poscnt++ ;												// --- 何回上下するかのカウントで使用
				// --- どちらも下に降りてきたときにそこで停止させる
				if(flg != 0){
					sp_scale[i] = 0.0f;
					char_pos_spd[i] = 0.0f; 
				}
			}

			// --- 台のスケールの値+キャラの上下の動き
			End_Stage_Scale[i] += sp_scale[i] ;
			char_pos_y[i] += char_pos_spd[i] ;
		}
	}

	// --- 始まりのバーのばらつきを生む（右側）
	if(poscnt == 0 && End_Stage_Scale[0] < 7.0f && sp_scale[0] > 0){
		sp_scale[1] = 0.0f;
		char_pos_spd[1] = 0.0f;
	}
	else{
		// --- 左が上に伸びきったら開始
		if(poscnt == 0 && End_Stage_Scale[0] >= 8.0f && sp_scale[0] > 0){
			sp_scale[1] = 0.3f;
			char_pos_spd[1] = 19.0f;
		}
	}

	// --- win/loseの表示+エフェクト付与
	if(	t_Player[1].animflg == pWin ){
		// --- リザルト文字の削除
		DeleteGraph( End_BG ) ;

		// --- 横から出るエフェクトの描画
		if(change_effect_flg != 1 && timer.re_time >= 8){
			// --- エフェクトの動き
			effect_pos_x[0] += effect_spd_x ;
			effect_pos_x[1] -= effect_spd_x ;
			effect_pos_y[0] -= effect_spd_y ;

			// --- 左側
			DrawGraph( effect_pos_x[0] , effect_pos_y[0] , End_Effect[1] , true ) ;
			DrawGraph( effect_pos_x[0] , (effect_pos_y[0]+300) , End_Effect[1] , true ) ;
			DrawGraph( effect_pos_x[0] , (effect_pos_y[0]+700) , End_Effect[1] , true ) ;

			// --- 右側
			DrawGraph( effect_pos_x[1] , effect_pos_y[0] , End_Effect[0] , true ) ;
			DrawGraph( effect_pos_x[1] , (effect_pos_y[0]+300) , End_Effect[0] , true ) ;
			DrawGraph( effect_pos_x[1] , (effect_pos_y[0]+700) , End_Effect[0] , true ) ;
		}

		// --- WIN文字のモデル描画
		MV1SetScale( Win_model , VGet( 3.1f , 3.0f , 3.0f) ) ;
		MV1SetPosition(Win_model , VGet(500.0f , 0.0f  , -300.0f) ) ;
		MV1DrawModel( Win_model ) ;

		// --- LOSE文字のモデル描画
		MV1SetScale( Lose_model , VGet( 2.0f , 2.0f , 2.0f) ) ;
		MV1SetPosition(Lose_model , VGet(-100.0f , 600.0f  , -300.0f) ) ;
		MV1DrawModel( Lose_model ) ;
	}
	else{
		if(	t_Player[0].animflg == pWin ){
			// --- リザルト文字の削除
			DeleteGraph( End_BG ) ;

			// --- 横から出るエフェクトの描画
			if(change_effect_flg != 1 && timer.re_time >= 8){
				// --- エフェクトの動き
				effect_pos_x[0] += effect_spd_x ;
				effect_pos_x[1] -= effect_spd_x ;
				effect_pos_y[0] -= effect_spd_y ;

				// --- 左側
				DrawGraph( effect_pos_x[0] , effect_pos_y[0] , End_Effect[1] , true ) ;
				DrawGraph( effect_pos_x[0] , (effect_pos_y[0]+300) , End_Effect[1] , true ) ;
				DrawGraph( effect_pos_x[0] , (effect_pos_y[0]+700) , End_Effect[1] , true ) ;

				// --- 右側
				DrawGraph( effect_pos_x[1] , effect_pos_y[0] , End_Effect[0] , true ) ;
				DrawGraph( effect_pos_x[1] , (effect_pos_y[0]+300) , End_Effect[0] , true ) ;
				DrawGraph( effect_pos_x[1] , (effect_pos_y[0]+700) , End_Effect[0] , true ) ;
			}

			// --- WIN文字のモデル描画
			MV1SetScale( Win_model , VGet( 3.1f , 3.0f , 3.0f) ) ;
			MV1SetPosition(Win_model , VGet(-300.0f , 0.0f  , -300.0f) ) ;
			MV1DrawModel( Win_model ) ;

			// --- LOSE文字のモデル描画
			MV1SetScale( Lose_model , VGet( 2.0f , 2.0f , 2.0f) ) ;
			MV1SetPosition(Lose_model , VGet(450.0f , 600.0f  , -300.0f) ) ;
			MV1DrawModel( Lose_model ) ;
		}
	}

	// --- 台の位置＋スケール
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		MV1SetScale( End_Stage[i] , VGet( 3.1f , End_Stage_Scale[i] , 3.0f) ) ;  // ステージ
		MV1SetPosition(End_Stage[i] , es_pos[i] ) ;
		MV1DrawModel( End_Stage[i] ) ;
	}

	// --- プレイヤー描画＋位置
	for ( int i = 0 ; i < MAX_PLAYER ; i++ ){
		MV1SetScale( t_Player[i].p_Model , VGet( 1.0f , 1.0f , 1.0f) ) ;
		MV1SetAttachAnimTime(t_Player[i].p_Model, t_Player[i].tmp_attachidx ,t_Player[i].anim_playtime) ;		// アニメーションの何番目（何秒後）の動きか同期させる
		MV1SetRotationXYZ( t_Player[i].p_Model, VGet(0.0f, 1.57f * t_Player[i].char_rot , 0.0f ) ) ;
		MV1SetPosition(t_Player[i].p_Model , VGet(es_pos[i].x , char_pos_y[i] , es_pos[i].z) ) ;	
		MV1DrawModel(t_Player[i].p_Model) ;
	}

	// --- effectの降下タイミング -----------------------------------------------------------
	if(effect_pos_y[0] <= -1500 && ((change_angle%2) == 1) ){
		change_effect_flg = 1 ;
		effect_pos_y[1] += effect_spd_y ;
		for(int i = 0 ; i < 5 ; i++ ){
			DrawGraph( (384*i) , (effect_pos_y[1]-(300*i)) , End_Buck_Effect[i] , true ) ;						// キャラの前に出てくるエフェクトの描画
		}
	}
	if(effect_pos_y[0] <= -1500 && ((change_angle%2) == 0)){
		change_effect_flg = 1 ;
		effect_pos_y[1] += effect_spd_y ;
		for(int i = 0 ; i < 5 ; i++ ){
			DrawGraph( (384*i) , (effect_pos_y[1]+(300*i)) , End_Buck_Effect[i] , true ) ;						// キャラの前に出てくるエフェクトの描画
		}
	}

	// --- 降ってくるエフェクトのYポジが2000まで降りたら再セット
	if(effect_pos_y[1] >= 2000){
		change_effect_flg = 0 ;
		change_angle++ ;
		effect_pos_x[0] = -1200 ;
		effect_pos_x[1] = 1920 ;
		effect_pos_y[0] = 1000 ;
		effect_pos_y[1] = -2000;
	}
	// -----------------------------------------------------------------------------------------

	// --- 黒幕のxポジを足す
	if(kuromaku_flg != false){
		maku_pos_x -= maku_spd_x ;
	}

	// --- 幕
	DrawBox(maku_pos_x,0,(maku_pos_x+2020),154,GetColor(         255 , 0 , 0),true) ;			//最後の引数をfalseにすると塗りつぶし無し(赤)
	DrawBox((maku_pos_x+100),154,(maku_pos_x+2120),308,GetColor( 255 ,165 ,0),true) ;			//最後の引数をfalseにすると塗りつぶし無し(赤)
	DrawBox((maku_pos_x+200),308,(maku_pos_x+2220),462,GetColor( 255 ,255 ,0),true) ;			//最後の引数をfalseにすると塗りつぶし無し(赤)
	DrawBox((maku_pos_x+300),462,(maku_pos_x+2320),616,GetColor( 0   ,128 ,0),true) ;			//最後の引数をfalseにすると塗りつぶし無し(赤)
	DrawBox((maku_pos_x+400),616,(maku_pos_x+2420),770,GetColor( 0   ,255 ,255),true) ;			//最後の引数をfalseにすると塗りつぶし無し(赤)
	DrawBox((maku_pos_x+500),770,(maku_pos_x+2520),924,GetColor( 0   ,0   ,255),true) ;			//最後の引数をfalseにすると塗りつぶし無し(青)
	DrawBox((maku_pos_x+600),924,(maku_pos_x+2620),1080,GetColor(128 ,0   ,128),true) ;			//最後の引数をfalseにすると塗りつぶし無し(緑)

}




